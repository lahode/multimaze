/**
 * Require librairies for node.js
 */
require('./lib/sprite.js');
require('./lib/actions.js');
require('./lib/collision.js');
require('./lib/mazegenerator.js');
 
/**
 * Define an object to hold all our images for the game so images
 * are only ever created once. This type of object is known as a
 * singleton.
 */
var imageRepository = new function()
{
	// Initialize the variables
	var numImages = 0;
	var numLoaded = 0;
  var self = this;
  this.start = false;

	// Define images
  this.images = new Array();
  
  // Add new image
  this.add = function(name, source)
  {
    // Check if an image already exist with the same name
    if (name in this.images)
    {
      throw ('Image already exist for ' + name);
    }
    
    // Initialize the new image and add it to the image array
    var newImage = new Image();
    newImage.src = source;
    newImage.onload = function()
    {
      imageLoaded();
    }
    this.images[name] = newImage;
    numImages++;
  }
  
  // Get image
  this.get = function(name) 
  {
    return this.images[name];
  }
  
	// Ensure all images have loaded before starting the game
  function imageLoaded()
  {
		numLoaded++;
		if (numLoaded === numImages && self.start) 
    {
      game.play = true;
		}
	}
}

/**
 * Game configuration class
 */
function GameConfiguration()
{
  this.spriteSize = 48;
  this.screenSize = {width: 640, height: 640};
  this.mazeSize = {width: 20, height: 20};
  this.speed = 4;
  this.bonusBoxNumber = 0;
  this.timePlay = 180;
}

/**
 * Array shuffle
 */
function shuffleArray (array) {
  var i = 0;
  var j = 0;
  var temp = null;

  for (i = array.length - 1; i > 0; i -= 1)
  {
    j = Math.floor(Math.random() * (i + 1));
    temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }
}  

/**
 * Game class
 */
function Game(configuration, client)
{

  // Initialize global properties
  this.config = configuration; // serv
  this.mainLayer = new Layer(this.config.mazeSize); // serv
  this.overLayer = new Layer(this.config.mazeSize); // serv
  this.maze = new Maze(this.config.mazeSize.width, this.config.mazeSize.height); // no
  this.debugSprite = []; // no
  this.scroll = []; // no
  this.explosion = []; // no
  this.bonusBox = new Array(); // no
  this.bonus = new Array(); // no
  this.gates = new Array(); // no
  this.treasure = []; // no
  this.play = false; // ?
  this.enableAction = false; // no
  this.gameAnimation = {state: 'start', winner: {}, startTimerPlayer: 0, startTimerGate: 0, timerPlayer: 0, timerGate: 0} // no

  // Initialize internal variables
  var self = this;
  var showPath; // To be checked
  
  /**
   * Function to initialize the maze
   */
  this.initWorldMap = function(characters)
  {
    // Initialize variables
    var debug = false;
    var spriteSize = this.config.spriteSize;

    // Initialize the debugger
    this.debugSprite = new Sprite('debug', {x: 0, y: 0}, null, spriteSize);
  
    // Initialize the maze
    imageRepository.add('wall', 'images/wall48.png');
    this.maze.draw(this.mainLayer, characters);
    
    // Add treasure
    imageRepository.add('treasure', 'images/treasure48.png');
    this.treasure = new Sprite('treasure', {x: this.config.mazeSize.width, y: this.config.mazeSize.height}, imageRepository.get('treasure'), spriteSize);
    this.overLayer.setGrid(this.treasure.name, this.treasure.position.x, this.treasure.position.y);
    
    // Add bonus box
    imageRepository.add('bonusBox', 'images/bonus48.png');
    var testX;
    var testY;
    for (var i=0; i<this.config.bonusBoxNumber;i++)
    {
      do {
        testX = Math.floor((Math.random() * (2 * this.config.mazeSize.width - 2)) + 1);
        testY = Math.floor((Math.random() * (2 * this.config.mazeSize.height - 2)) + 1);
      }
      while (this.mainLayer.grid[testY][testX] != 'empty' || this.overLayer.grid[testY][testX] != 'empty');
      this.bonusBox[i] = new AnimatedSprite('bonusBox', {x: testX, y: testY}, imageRepository.get('bonusBox'), spriteSize, 10, 40, 0);
      this.overLayer.setGrid(this.bonusBox[i].name, this.bonusBox[i].position.x, this.bonusBox[i].position.y);
    }
    
    // Add the bonuses
    imageRepository.add('bomb', 'images/bomb48.png');
    this.bonus[0] = new AnimatedSprite('bomb', {x: 0, y: 0}, imageRepository.get('bomb'), spriteSize, 6, 500, 1);
    imageRepository.add('star', 'images/star48.png');
    this.bonus[1] = new Sprite('star', {x: 0, y: 0}, imageRepository.get('start'), spriteSize);
    
    // Define explosion
    imageRepository.add('explosion', 'images/explosion.png');
    this.explosion = new AnimatedSprite('explosion', {x: 0, y: 0}, imageRepository.get('explosion'), spriteSize, 16, 50, 1);
    this.explosion.collision = new Collision(this, this.explosion);
    
    // Add gates
    imageRepository.add('gateClose', 'images/gateclose48.png');
    imageRepository.add('gateOpen', 'images/gateopen48.png');
    this.gates[0] = new AnimatedSprite('gate', {x: 0, y: 21}, imageRepository.get('gateClose'), spriteSize, 6, 600, 1);
    this.gates[1] = new AnimatedSprite('gate', {x: 40, y: 21}, imageRepository.get('gateClose'), spriteSize, 6, 600, 1);
    this.gates[2] = new AnimatedSprite('gate', {x: 21, y: 0}, imageRepository.get('gateClose'), spriteSize, 6, 600, 1);
    this.gates[3] = new AnimatedSprite('gate', {x: 21, y: 40}, imageRepository.get('gateClose'), spriteSize, 6, 600, 1);

    // Ready to start the game
    // imageRepository.start = true;
  }

  /**
   * Finish animation
   */
  this.finishAnimation = function()
  {
    if (self.gameAnimation.startTimerGate == 0)
    {
      self.enableAction = false;
      self.gameAnimation.winner.axion.actionList.move.active = false;
      switch (self.gameAnimation.winner.direction)
      {
        case 'N' : 
          self.gameAnimation.winner.axion.actionList.autoMove.end = {x: self.gameAnimation.winner.position.x, y: self.gameAnimation.winner.position.y - 1};
          self.mainLayer.removeFrom(self.gameAnimation.winner.position.x, self.gameAnimation.winner.position.y - 1);
          break;
        case 'S' : 
          self.gameAnimation.winner.axion.actionList.autoMove.end = {x: self.gameAnimation.winner.position.x, y: self.gameAnimation.winner.position.y + 1};
          self.mainLayer.removeFrom(self.gameAnimation.winner.position.x, self.gameAnimation.winner.position.y + 1);
          break;
        case 'W' :
          self.gameAnimation.winner.axion.actionList.autoMove.end = {x: self.gameAnimation.winner.position.x - 1, y: self.gameAnimation.winner.position.y};
          self.mainLayer.removeFrom(self.gameAnimation.winner.position.x - 1, self.gameAnimation.winner.position.y);
          break;
        case 'E' : 
          self.gameAnimation.winner.axion.actionList.autoMove.end = {x: self.gameAnimation.winner.position.x + 1, y: self.gameAnimation.winner.position.y};
          self.mainLayer.removeFrom(self.gameAnimation.winner.position.x + 1, self.gameAnimation.winner.position.y);
          break;
      }
      this.endGate = new AnimatedSprite('gate', self.gameAnimation.winner.axion.actionList.autoMove.end, imageRepository.get('gateOpen'), 6, 400, 1);
      self.mainLayer.push(this.endGate);
      self.gameAnimation.timerGate = this.endGate.timer * this.endGate.frames;
      self.gameAnimation.startTimerGate = Date.now();
    }
    else if (self.gameAnimation.startTimerPlayer == 0 && self.gameAnimation.startTimerGate + self.gameAnimation.timerGate < Date.now())
    {
      self.gameAnimation.winner.axion.actionList.autoMove.active = true;
      self.gameAnimation.winner.axion.actionList.autoMove.start = self.gameAnimation.winner.position;
      self.gameAnimation.winner.axion.actionList.autoMove.timer = 50;
      self.gameAnimation.timerPlayer = self.gameAnimation.winner.axion.actionList.autoMove.timer * 12;
      self.gameAnimation.startTimerPlayer = Date.now();
    }
    else if (self.gameAnimation.startTimerPlayer > 0 && self.gameAnimation.startTimerPlayer + self.gameAnimation.timerPlayer < Date.now())
    {
      console.log(self.gameAnimation.winner.name + ' wins !');      
      self.play = false;
    }
  }
}

/**
 * Define all characters in the game
 */
var worldCharacters = function(world)
{
  // Initialize variables
  var spriteSize = world.config.spriteSize;
  var NESW = ["N", "E", "S", "W"];
  var characters = new Array();

  // Shuffle characters positions
  shuffleArray(NESW);

  // Initialize character n�1
  imageRepository.add('character1_moveRight', 'images/GentillePoops_Poopy_RunRightCycle.png');
  imageRepository.add('character1_moveLeft', 'images/GentillePoops_Poopy_RunLeftCycle.png');
  imageRepository.add('character1_moveUp', 'images/GentillePoops_Poopy_RunBackCycle.png');
  imageRepository.add('character1_moveDown', 'images/GentillePoops_Poopy_RunFrontCycle.png');
  imageRepository.add('character1_dropOrCatch', 'images/GentillePoops_Poopy_DropThing.png');
  imageRepository.add('character1_waiting', 'images/GentillePoops_Poopy_Waiting.png');
  characters['Poopy'] = new Character(world, 'Poopy', {x: 0, y: 0}, NESW[0]);
  characters['Poopy'].addSprite('DC', imageRepository.get('character1_dropOrCatch'));
  characters['Poopy'].addSprite('E', imageRepository.get('character1_moveRight'));
  characters['Poopy'].addSprite('N', imageRepository.get('character1_moveUp'));
  characters['Poopy'].addSprite('S', imageRepository.get('character1_moveDown'));
  characters['Poopy'].addSprite('W', imageRepository.get('character1_moveLeft'));
  characters['Poopy'].addSprite('', imageRepository.get('character1_waiting'));

  // Initialize character n�1
  imageRepository.add('character2_moveRight', 'images/VilainePoops_Poopy_RunRightCycle.png');
  imageRepository.add('character2_moveLeft', 'images/VilainePoops_Poopy_RunLeftCycle.png');
  imageRepository.add('character2_moveUp', 'images/VilainePoops_Poopy_RunBackCycle.png');
  imageRepository.add('character2_moveDown', 'images/VilainePoops_Poopy_RunFrontCycle.png');
  imageRepository.add('character2_dropOrCatch', 'images/VilainePoops_Poopy_DropThing.png');
  imageRepository.add('character2_waiting', 'images/VilainePoops_Poopy_Waiting.png');
  characters['Blondy'] = new Character(world, 'Blondy', {x: 1, y: 11}, NESW[1]);
  characters['Blondy'].addSprite('DC', imageRepository.get('character2_dropOrCatch'));
  characters['Blondy'].addSprite('E', imageRepository.get('character2_moveRight'));
  characters['Blondy'].addSprite('N', imageRepository.get('character2_moveUp'));
  characters['Blondy'].addSprite('S', imageRepository.get('character2_moveDown'));
  characters['Blondy'].addSprite('W', imageRepository.get('character2_moveLeft'));
  characters['Blondy'].addSprite('', imageRepository.get('character2_waiting'));

  // Initialize character n�3
  imageRepository.add('character3_moveRight', 'images/Ninja_Poopy_RunRightCycle.png');
  imageRepository.add('character3_moveLeft', 'images/Ninja_Poopy_RunLeftCycle.png');
  imageRepository.add('character3_moveUp', 'images/Ninja_Poopy_RunBackCycle.png');
  imageRepository.add('character3_moveDown', 'images/Ninja_Poopy_RunFrontCycle.png');
  imageRepository.add('character3_dropOrCatch', 'images/Ninja_Poopy_DropThing.png');
  imageRepository.add('character3_waiting', 'images/Ninja_Poopy_Waiting.png');
  characters['Ninja'] = new Character(world, 'Ninja', {x: 1, y: 31}, NESW[2]);
  characters['Ninja'].addSprite('DC', imageRepository.get('character3_dropOrCatch'));
  characters['Ninja'].addSprite('E', imageRepository.get('character3_moveRight'));
  characters['Ninja'].addSprite('N', imageRepository.get('character3_moveUp'));
  characters['Ninja'].addSprite('S', imageRepository.get('character3_moveDown'));
  characters['Ninja'].addSprite('W', imageRepository.get('character3_moveLeft'));
  characters['Ninja'].addSprite('', imageRepository.get('character3_waiting'));

  // Initialize character n�4
  imageRepository.add('character4_moveRight', 'images/Franky_Poopy_RunRightCycle.png');
  imageRepository.add('character4_moveLeft', 'images/Franky_Poopy_RunLeftCycle.png');
  imageRepository.add('character4_moveUp', 'images/Franky_Poopy_RunBackCycle.png');
  imageRepository.add('character4_moveDown', 'images/Franky_Poopy_RunFrontCycle.png');
  imageRepository.add('character4_dropOrCatch', 'images/Franky_Poopy_DropThing.png');
  imageRepository.add('character4_waiting', 'images/Franky_Poopy_Waiting.png');
  characters['Franky'] = new Character(world, 'Franky', {x: 39, y: 11}, NESW[3]);
  characters['Franky'].addSprite('DC', imageRepository.get('character4_dropOrCatch'));
  characters['Franky'].addSprite('E', imageRepository.get('character4_moveRight'));
  characters['Franky'].addSprite('N', imageRepository.get('character4_moveUp'));
  characters['Franky'].addSprite('S', imageRepository.get('character4_moveDown'));
  characters['Franky'].addSprite('W', imageRepository.get('character4_moveLeft'));
  characters['Franky'].addSprite('', imageRepository.get('character4_waiting'));
  
  // Shuffle characters
  shuffleArray(characters);

  return characters;
}

var gameMaze = gameEngine;
//var playerMaze = gamePlayer;

/**
 * Update on the client side
 */
gameMaze.prototype.client_update = function()
{
  var world = this.world;
  var context = this.ctx;
  
  // Clean the canvas
  context.clearRect(0, 0, world.config.screenSize.width, world.config.screenSize.height);

  // Capture inputs from the player
  this.client_handle_input();

  // Draw the path
  if (world.showPath && "items" in world.showPath)
  {
    for(var i in world.showPath.items) 
    {
      context.fillRect(world.showPath.items[i].x * spriteSize + world.scroll.x + Math.floor(spriteSize / 4), world.showPath.items[i].y * spriteSize + world.scroll.y +  Math.floor(spriteSize / 4), Math.floor(spriteSize / 2), Math.floor(spriteSize / 2));
      world.showPath.items
    }
    if ((world.showPath.startTime+3000) < Date.now()) {
      world.showPath = null;
    }
  }
    
  // Draw each overlay sprites on the canvas
  for (var i in world.overLayer.elements)
  {
    world.overLayer.elements[i].update();
    world.overLayer.elements[i].draw(context, world.scroll);
  }    

  // Draw each sprites on the canvas
  for (var i in world.mainLayer.elements)
  {
    world.mainLayer.elements[i].update();
    world.mainLayer.elements[i].draw(context, world.scroll);
  }
  
  // Manage starting and ending animation
  switch (this.world.gameAnimation.state)
  {
    case 'finish' :
      this.world.finishAnimation();
      break;
  }  
}

/**
 * Update on the server side
 *
 * Makes sure things run smoothly and 
 * notifies clients of changes
 */
gameMaze.prototype.server_update = function()
{
  // Update the state of our local clock to match the timer
  this.server_time = this.local_time;
  this.laststate = {t : this.server_time, players : {}};
  var players = this.server_players;

  // Make a snapshot of the current state, for updating the clients
  for (var uid in players)
  {
    this.laststate.players[uid] = {
      graphPosition : players[uid].graphPosition,       // player position
      direction : players[uid].direction,               // player direction
      carry : players[uid].carry,                       // player carry
      last_input_seq : players[uid].last_input_seq      // player last inputs sequence
    }; 
  }
  
  this.laststate.world = {
    overLayer: this.world.overLayer.grid,
    mainLayer: this.world.mainLayer.grid
  }  
  
  // Send the snapshot to the all players
  for (var uid in players)
  {
    players[uid].instance.emit('onserverupdate', this.laststate);
    var time = new Date();
    time.setTime((this.world.config.timePlay + this.waitingTime - this.server_time) * 1000);
    players[uid].instance.send('s.m.timer.' + ("0" + (time.getMinutes())).slice(-2) + ':'+ ("0" + (time.getSeconds())).slice(-2));    
  }
  
  // If the timer is finished, end the game
  if (this.world.gameAnimation.state != 'ended' && this.server_time - this.waitingTime >= this.world.config.timePlay)
  {
    // End the game
    game_server.endGame(this.instance.id, this.instance.player_host.userid);
    this.world.gameAnimation.state = 'ended';
    for (var uid in players)
    {
      players[uid].instance.send('s.m.message.' + 'Le temps est écoulé, Game over!');
    }
  }
  
  // Manage the end of the game
  switch (this.world.gameAnimation.state)
  {
    case 'finish' :

    // End the game
    game_server.endGame(this.instance.id, this.instance.player_host.userid);
    this.world.gameAnimation.state = 'ended';
    for (var uid in players)
    {
      players[uid].instance.send('s.m.message.' + this.world.gameAnimation.winner.name + ' a gagné!');
    }
  }   
  
}; //gameMaze.server_update

/**
 * Update the game on the client side
 */
gameMaze.prototype.client_onserverupdate_received = function(data)
{
  for (var uid in data.players)
  {
    if (this.client_players.self.id == uid)
    {
      for (var command in data.players[uid])
      {
        this.client_players.self[command] = data.players[uid][command];
      }
    }
    else
    {
      for (var command in data.players[uid])
      {
        if (this.client_players.others[uid] && 'graphPosition' in this.client_players.others[uid])
        {
          this.client_players.others[uid][command] = data.players[uid][command];
        }
      }
    }
  }
  
  // Update world
  this.world.mainLayer.grid = data.world.mainLayer;    
  this.world.overLayer.grid = data.world.overLayer;    

}; //gameMaze.client_onserverupdate_received

/**
 * Display a message comming from the server
 */
gameMaze.prototype.client_showmessage = function(type, text)
{
  switch(type)
  {
    // Show the waiting message
    case 'waiting' :
      $('#waiting-message').html('Le jeu commence dans ' + text + ' secondes');
      break;

    // Manage starting message
    case 'starting' :
      $('#instances').html(' ');
      $('#waiting-message').html('Partez!');
      $('#waiting-players').hide(1000);
      break;

    // Show the waiting message
    case 'message' :
      $('.game-message').html(text);
      break;
      
    case 'timer' :
      $('#timer').html(text);
      break;

    // Show the waiting message
    case 'disconnected' :
      $('.game-message').html('Le joueur ' + text + ' a quitt� la partie');
      console.log('message : ' + 'Le joueur ' + text + ' a quitt� la partie')
      break;      
  }
}

/**
 * Show available instances
 */
gameMaze.prototype.client_showinstances = function(instances)
{
  var self = this;
  var instances = JSON.parse(instances);
  if (instances[0])
  {
    // Define a random instance to join
    shuffleArray(instances);

    // Show the join button and add a new click listener
    $('#instances').html('<a id="join" href="#game" class="ui-link">Rejoindre une partie</a>');
    $('#join').on('click', function() {
      self.client_create_join_game(instances[0]);
    });
  }
  else
  {
    $('#instances').html(' ');
  }
}

/**
 * When we disconnect, we don't know if the other player is
 * connected or not, and since we aren't, everything goes to offline
 */
gameMaze.prototype.client_ondisconnect = function(data)
{
  // Update player status
  this.client_players.self.state = 'not-connected';
  this.client_players.self.online = false;
  
  // Stop the game updates immediate
  this.stop_update();
  
  $("#waiting-players").show();
  $("body").pagecontainer('change', 'index.html', {transition: 'flow',reload: false});

}; //client_ondisconnect

/**
 * Handle input from the client
 *
 * This takes input from the client and keeps a record,
 * It also sends the input information to the server immediately
 * as it is pressed. It also tags each input with a sequence number.
 */
gameMaze.prototype.client_handle_input = function(newInput)
{
//  if (!this.world.enableAction) return;
  
  // Initialize variables
  var input = [];
  
  // Manage action from the key pressed
  if (this.keyboard.pressed('left'))
  {
    input.push('ml');
    /*
    this.axion.actionList.move.direction = 'W';
    this.axion.actionList.move.active = true;
    this.currentSprite = this.animatedSprites['W'];
    */
  }
  if (this.keyboard.pressed('right'))
  {
    input.push('mr');
    /*
    this.axion.actionList.move.direction = 'E';
    this.axion.actionList.move.active = true;
    this.currentSprite = this.animatedSprites['E'];
    */
  }
  if (this.keyboard.pressed('down'))
  {
    input.push('md');
    /*
    this.axion.actionList.move.direction = 'S';
    this.axion.actionList.move.active = true;
    this.currentSprite = this.animatedSprites['S'];
    */
  }
  if (this.keyboard.pressed('up'))
  {
    input.push('mu');
    /*
    this.axion.actionList.move.direction = 'N';
    this.axion.actionList.move.active = true;
    this.currentSprite = this.animatedSprites['N'];
    */
  }
  if (this.keyboard.pressed('space'))
  {
    input.push('dc');
    /*
    this.axion.actionList.dropOrCatch.frame = 0;
    this.axion.actionList.dropOrCatch.active = true;
    this.currentSprite = this.animatedSprites['DC'];
    */
  }
  if (this.keyboard.pressed('tab'))
  {
    input.push('ui');
    /*
    this.axion.actionList.useInventory.frame = 0;
    this.axion.actionList.useInventory.active = true;
    */
  }
  if (newInput)
  {    
    input.push(newInput);
  }
  
  // Store the inputs and send it to the server
  if(input.length) {

    // Update what sequence we are on now
    this.input_seq += 1;

    // Store the input state as a snapshot of what happened.
    this.client_players.self.inputs.push({
        inputs : input,
        time : this.local_time.fixed(3),
        seq : this.input_seq
    });

    // Send the packet of information to the server.
    // The input packets are labelled with an 'i' in front.
    var server_packet = 'i.';
        server_packet += input.join('-') + '.';
        server_packet += this.local_time.toFixed(3).replace('.','-') + '.';
        server_packet += this.input_seq;

    // Send the packet to the server
    this.socket.send(server_packet);
  }
}; //gameMaze.client_handle_input

/**
 * Process the input on the player (can be both, client & server side)
 *
 * -- To be redefined --
 */
gameMaze.prototype.process_input = function(player) {
  var result;

  // Check if there are inputs to process
  if (player.inputs.length)
  {
    player.axion.actionList.move.active = false;
    for(var j = 0; j < player.inputs.length; ++j)
    {
      var input = player.inputs[j].inputs;
      for(var i = 0; i < input.length; ++i)
      {
        switch(input[i])
        {
          case 'mr' :
            player.axion.actionList.move.direction = 'E';
            player.axion.actionList.move.active = true;
            break;
          case 'ml' :
            player.axion.actionList.move.direction = 'W';
            player.axion.actionList.move.active = true;
            break;
          case 'mu' :
            player.axion.actionList.move.direction = 'N';
            player.axion.actionList.move.active = true;
            break;
          case 'md' :
            player.axion.actionList.move.direction = 'S';
            player.axion.actionList.move.active = true;
            break;
          case 'dc' :
            if (this.world.overLayer.grid[player.position.y][player.position.x] != 'empty')
            {
              player.carry = this.world.overLayer.grid[player.position.y][player.position.x];
              this.world.overLayer.grid[player.position.y][player.position.x] = 'empty';
            }
            break;
            /*
            player.axion.actionList.dropOrCatch.frame = 0;
            player.axion.actionList.dropOrCatch.active = true;
            break;
          case 'ui' :
            player.axion.actionList.useInventory.frame = 0;
            player.axion.actionList.useInventory.active = true;
            break;
          case 'gb' :
            player.axion.grabBonus;
            this.world.overLayer.grid[player.position.y][player.position.x] = 'empty';
            break;
            */
        }
      }
    }
    
    // Execute the character action
    for (var actionName in player.axion.actionList)
    {
      if (player.axion.actionList[actionName].active)
      {
        player.axion[actionName]();
      }
    }
    
    // We can now clear the array since these have been processed
    while(player.inputs.length > 0) {player.inputs.pop();}
  }    
  
} //gameMaze.process_input

/**
 * Initialize the maze game
 */
gameMaze.prototype.client_initWordGame = function() {
  this.world = new Game(new GameConfiguration(), true);
  this.characters = worldCharacters(this.world);
  this.world.initWorldMap(this.characters);
} //gameMaze.client_initWordGame

/**
 * Configure world with host player configuration
 *
 * -- To be redefined -- 
 */
gameMaze.prototype.server_initWordGame = function(gameInfo) {
  var gameInfo = gameInfo ? JSON.parse(gameInfo) : [];
  this.world = [];
  this.world.config = gameInfo.config;
  this.world.mainLayer = new Layer(this.world.config.mazeSize);
  this.world.mainLayer.grid = gameInfo.mainLayerGrid;
  this.world.overLayer = new Layer(this.world.config.mazeSize);
  this.world.overLayer.grid = gameInfo.overLayerGrid;
  this.world.gameAnimation = {state: 'start', winner: {}};

  // Add gates
  this.world.mainLayer.grid[21][0] = 'gate';
  this.world.mainLayer.grid[40][21] = 'gate';
  this.world.mainLayer.grid[0][21] = 'gate';
  this.world.mainLayer.grid[40][40] = 'gate';
  
  this.world.maze = this.world.config.mazeSize;
} //gameMaze.server_initWordGame

/**
 * Add new player when player is joining
 */
gameMaze.prototype.server_add_new_player = function(playerInfo) {
  var playerInfo = playerInfo ? JSON.parse(playerInfo) : [];
  var player = new gamePlayer(this, this.instance.players[playerInfo.id]);
  player.init();
  player.id = playerInfo.id;
  player.name = playerInfo.name;
  player.direction = playerInfo.direction;
  player.position = playerInfo.position;
  player.changePosition();
  this.server_players[player.id] = player;
} //gameMaze.server_add_new_player

/**
 * Start the game
 */
gameMaze.prototype.client_create_join_game = function(gameID)
{
  var command = gameID ? 'j.' : 's.';
  this.socket.send(
    command + gameID + '.' + JSON.stringify({
      id: this.client_players.self.id,
      name: this.client_players.self.name,
      position: this.client_players.self.position,
      direction: this.client_players.self.direction
    }) + '.' + JSON.stringify({
      config: this.world.config,
      mainLayerGrid: this.world.mainLayer.grid,
      overLayerGrid: this.world.overLayer.grid
    })
  );
} //gameMaze.createNewGame

/**
 * Updating all players information before starting the game
 */
gameMaze.prototype.client_onreadygame = function(data) {

  var datas = JSON.parse(data);
  var server_time = parseFloat(datas.local_time.replace('-','.'));

  this.local_time = server_time + this.net_latency;
  console.log('server time is about ' + this.local_time);
  
  // Update our status
  this.client_players.self.state  = this.client_players.self.host ? 'local_pos(hosting)' : 'local_pos(joined)';

  // Add all other players and update their information
  for (var p in datas.players)
  {
    if (datas.players[p].id != this.client_players.self.id)
    {
      var other_player = new gamePlayer(this);
      other_player.init();
      other_player.id = datas.players[p].id;
      other_player.host = datas.players[p].host;
      other_player.state = datas.players[p].host ? 'local_pos(hosting)' : 'local_pos(joined)';
      other_player.online = true;           
      this.client_players.others[other_player.id] = other_player;
    }      
  }
  
  // Update the main and over layers grid
  this.world.overLayer.grid = datas.world.overLayer.grid;
  this.world.mainLayer.grid = datas.world.mainLayer.grid;
  this.world.overLayer.addElements(this.world);
  this.world.mainLayer.addElements(this.world);

  // Add players on the main layer and initialize last position
  this.world.mainLayer.push(this.client_players.self);
  this.client_players.self.scrollLock = false;
  this.client_players.self.last_graphPosition = {
    x : this.client_players.self.graphPosition.x,
    y : this.client_players.self.graphPosition.y
  }  
  for(var p in this.client_players.others)
  {
    this.world.mainLayer.push(this.client_players.others[p]);    
    this.client_players.others[p].scrollLock = true;
    this.client_players.others[p].last_graphPosition = {
      x : this.client_players.others[p].graphPosition.x,
      y : this.client_players.others[p].graphPosition.y
    }
  }
  
  // Initiate scroll
  switch(this.client_players.self.direction)
  {
    case 'W' : this.world.scroll = {x: 0 * this.world.config.spriteSize, y: -15 * this.world.config.spriteSize};break;
    case 'E' : this.world.scroll = {x: -28 * this.world.config.spriteSize, y: -15 * this.world.config.spriteSize};break;
    case 'S' : this.world.scroll = {x: -15 * this.world.config.spriteSize, y: -28 * this.world.config.spriteSize};break;
    case 'N' : this.world.scroll = {x: -15 * this.world.config.spriteSize, y: 0};break;
  }
  console.log(this.client_players.self.direction);
  console.log(this.world.scroll);
  
  // Set this flag, so that the update loop can run it.
  this.active = true;
  
  // Tell the client that the game has started
  this.client_showmessage('starting');
  
  // Start the loop
  this.update(new Date().getTime());

}; //gameMaze.client_onreadygame

var gamePlayerMaze = gamePlayer;

/**
 * Initiate the player
 */
gamePlayerMaze.prototype.init = function()
{ 
  var game_instance = this.game;
  
  if ('characters' in game_instance)
  {
    var firstKey = Object.keys(game_instance.characters)[0];
    var character = game_instance.characters[firstKey];
    delete(game_instance.characters[firstKey]);
  }
  else
  {
    // Create a new character
    var character = new Character(game_instance.world, '', {x: 0, y: 0}, '');
  }
  
  // Add the character to the player
  this.animatedSprites = character.animatedSprites;
  this.currentSprite = character.currentSprite;
  this.carrySprite = character.carrySprite;
  this.inventory = character.inventory;
  this.direction = character.direction;
  this.mortal = character.mortal;
  this.name = character.name;
  this.position = character.position;
  this.graphPosition = character.graphPosition;
  this.axion = new Actions(game_instance, this);
  this.collision = new Collision(game_instance, this);
  this.draw = character.draw;
  this.changePosition = character.changePosition;
  this.changeGraphPosition = character.changeGraphPosition;
  this.addSprite = character.addSprite;
  this.update = character.update;
  this.die = character.die;
  
}; //gamePlayer.constructor


// Server side we set the 'gameMaze' class to a global type, so that it can use it anywhere.
if( 'undefined' != typeof global ) {
  module.exports = global.gameMaze = gameMaze;
  module.exports = global.gamePlayerMaze = gamePlayerMaze;  
}
