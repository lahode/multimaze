function Collision(gameEngine, object)
{

  // Initialize the variables
  var self = this;
  var emptyObj = {};
  var game = gameEngine.world;
  this.object = object;
  
  // Return collision with other objects and launch the action related
  this.returnCollisionWithSprites = function(collision, layerName)
  {
    var name = game[layerName || 'mainLayer'].grid[collision.y][collision.x];
    if (name && name != self.object.name)
    {
      switch(name)
      {
        case 'wall' :
          return true;
          break;
        case 'gate' : 
          if (self.object.carry == 'treasure' || (
              self.object.carrySprite && self.object.carrySprite.name == 'treasure'))
              {
                game.gameAnimation.state = 'finish';
                game.gameAnimation.winner = self.object;
              }
              return true;
          break;
        case 'enemy' :
          self.object.die();
          break;
        case (name.match(/^player/)  || emptyObj).input :
          if (self.object.name.match(/^player/))
          {
            return false;
          }
          var obj = game.mainLayer.getFrom(collision.x, collision.y);
          obj.die();
          break;
        case 'bonusBox' :
          self.object.axion.grabBonus();
          console.log(game);
          gameEngine.client_handle_input('gb');
          break;
      }
    }
    return false;
  }

  // Check all possible collisions
  this.check = function (direction, noCollisionCheck)
  {
    // Initialize variable
    var collision = self.object.position;

    switch (direction)
    {
      case 'N' : // Up collision

        if ('graphPosition' in self.object)
        {
          // Align the character horizontally if the X position is in between cases
          if (self.object.graphPosition.x % game.config.spriteSize <= Math.floor(game.config.spriteSize / 2))
          {
            self.object.graphPosition.x -= self.object.graphPosition.x % game.config.spriteSize;
            self.object.changeGraphPosition();
          }
          else if (self.object.graphPosition.x % game.config.spriteSize > Math.floor(game.config.spriteSize / 2))
          {
            self.object.graphPosition.x -= (self.object.graphPosition.x % game.config.spriteSize) - game.config.spriteSize;
            self.object.changeGraphPosition();
          }

          // Set collision position check
          collision = {x: Math.floor(self.object.graphPosition.x / game.config.spriteSize),
                       y: Math.floor(self.object.graphPosition.y / game.config.spriteSize)};
        }
        else
        {
          collision.y -=1;
        }
        
        // If debug is active, show collision blocks
        if (game.debugSprite)
        {
          game.debugSprite.position.x = collision.x;
          game.debugSprite.position.y = collision.y;
        }

        // Return the type of collision detected
        if ('graphPosition' in self.object && self.object.position.y > 0)
        {
          return (noCollisionCheck || false) ? false : this.returnCollisionWithSprites(collision);
        }
        break;
      case 'S' : // Down collision

        if ('graphPosition' in self.object)
        {
          // Align the character horizontally if the X position is in between cases
          if (self.object.graphPosition.x % game.config.spriteSize <= Math.floor(game.config.spriteSize / 2))
          {
            self.object.graphPosition.x -= self.object.graphPosition.x % game.config.spriteSize;
            self.object.changeGraphPosition();
          }
          else if (self.object.graphPosition.x % game.config.spriteSize > Math.floor(game.config.spriteSize / 2))
          {
            self.object.graphPosition.x -= (self.object.graphPosition.x % game.config.spriteSize) - game.config.spriteSize;
            self.object.changeGraphPosition();
          }

          // Set collision position check
          collision = {x: Math.floor(self.object.graphPosition.x / game.config.spriteSize),
                       y: Math.floor((self.object.graphPosition.y + game.config.spriteSize) / game.config.spriteSize)};
        }
        else
        {
          collision.y +=1;
        }
                       
        // If debug is active, show collision blocks
        if (game.debugSprite)
        {
          game.debugSprite.position.x = collision.x;
          game.debugSprite.position.y = collision.y;
        }

        // Return the type of collision detected
        if ('graphPosition' in self.object && self.object.position.y <= game.maze.width * 2)
        {
          return (noCollisionCheck || false) ? false : this.returnCollisionWithSprites(collision);
        }
        break;
      case 'W' : // Left collision

        if ('graphPosition' in self.object)
        {
          // Align the character vertically if the Y position is in between cases
          if (self.object.graphPosition.y % game.config.spriteSize <= Math.floor(game.config.spriteSize / 2))
          {
            self.object.graphPosition.y -= self.object.graphPosition.y % game.config.spriteSize;
            self.object.changeGraphPosition();
          }
          else if (self.object.graphPosition.y % game.config.spriteSize > Math.floor(game.config.spriteSize / 2))
          {
            self.object.graphPosition.y -= (self.object.graphPosition.y % game.config.spriteSize) - game.config.spriteSize;
            self.object.changeGraphPosition();
          }

          // Set collision position check
          collision = {x: Math.floor(self.object.graphPosition.x / game.config.spriteSize),
                       y: Math.floor(self.object.graphPosition.y / game.config.spriteSize)};
        }
        else
        {
          collision.x +=1;
        }
                       
        // If debug is active, show collision blocks
        if (game.debugSprite)
        {
          game.debugSprite.position.x = collision.x;
          game.debugSprite.position.y = collision.y;
        }

        // Return the type of collision detected
        if ('graphPosition' in self.object && self.object.graphPosition.x > 0)
        {
          return (noCollisionCheck || false) ? false : this.returnCollisionWithSprites(collision);
        }
        break;
      case 'E' : // Right collision

        if ('graphPosition' in self.object)
        {
          // Align the character vertically if the Y position is in between cases
          if (self.object.graphPosition.y % game.config.spriteSize <= Math.floor(game.config.spriteSize / 2))
          {
            self.object.graphPosition.y -= self.object.graphPosition.y % game.config.spriteSize;
            self.object.changeGraphPosition();
          }
          else if (self.object.graphPosition.y % game.config.spriteSize > Math.floor(game.config.spriteSize / 2))
          {
            self.object.graphPosition.y -= (self.object.graphPosition.y % game.config.spriteSize) - game.config.spriteSize;
            self.object.changeGraphPosition();
          }

          // Set collision position check
          collision = {x: Math.floor((self.object.graphPosition.x + game.config.spriteSize) / game.config.spriteSize),
                       y: Math.floor(self.object.graphPosition.y / game.config.spriteSize)};
        }
        else
        {
          collision.x -=1;
        }
                       
        // If debug is active, show collision blocks
        if (game.debugSprite)
        {
          game.debugSprite.position.x = collision.x;
          game.debugSprite.position.y = collision.y;
        }

        // Return the type of collision detected
        if ('graphPosition' in self.object && self.object.position.x <= game.maze.width * 2)
        {
          return (noCollisionCheck || false) ? false : this.returnCollisionWithSprites(collision);
        }
        break;
        
      default : // Check position on mainLayer and overLayer
        collision = self.object.position;
        var onMainLayer = this.returnCollisionWithSprites(collision);
        var onoverLayer = this.returnCollisionWithSprites(collision, 'overLayer');
        return onMainLayer || onoverLayer;
      
    }
    return true;
  }
}

// Server side we set the 'Collision' class to a global type, so that it can use it anywhere.
if( 'undefined' != typeof global ) {
  module.exports = global.Collision = Collision;
}
