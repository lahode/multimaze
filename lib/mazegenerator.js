/* Represents a maze. The maze is a path through a grid, with some of the grid's
 * walls missing. The dimensions of the grid (the width and height) are in terms
 * of these cells.
 */
function Maze(width, height)
{
  
  this.width = width;
  this.height = height;
  
  var cells = [];
  var visited = 0; // The number of cells already visited.
  var solution = []; // The solution to the maze.
  
  // Initiate the cell array
  for (var x = 0; x < width; x++) 
  {
    cells[x] = [];
    for (var y = 0; y < height; y++)
    {
      cells[x][y] = new Cell(x, y);
    }
  }

  /* Gets all of the cells we can possibly go to next. If the second argument
   * is true, this takes walls into account when finding options, returning
   * only the options with no walls between them and the cell.
   */
  function getOptions(cell, walls) 
  {
    var options = [];
    
    /* If the given coordinates are an option, pushes the corresponding 
     * cell onto the options array.
     */
    function checkOption(x, y) 
    {
      if (walls)
      {
        if ((y < cell.y && cell.walls.n) || (x > cell.x && cell.walls.e) || (y > cell.y && cell.walls.s) || (x < cell.x && cell.walls.w)) 
        {
          return;
        }
      }   

      if (cells[x] && cells[x][y] && !cells[x][y].visited)
      {
        options.push(cells[x][y]);
      }
    }

    checkOption(cell.x, cell.y - 1);
    checkOption(cell.x + 1, cell.y); 
    checkOption(cell.x, cell.y + 1); 
    checkOption(cell.x - 1, cell.y);

    return options;
  }
  
  /* Visits each unvisited cell, starting with the given one, randomly
   * traveling to adjacent cells and knocking out the walls between them to
   * create the maze. The optional direction argument is the direction the
   * visit _came from_. That is, if you visit cell (1, 2) from cell (0, 2),
   * the direction would be "e" (east).
   */
  function visit(cell, direction) 
  {
    cell.visited = true;
    if (direction)
    {
      cell.walls[direction] = false;
    }

    //Get the possible options for going forward:
    var options = getOptions(cell);

    while (options.length > 0)
    {
      // The next cell is chosen randomly from the possible options
      var index = Math.floor(Math.random() * options.length);
      var nextCell = options[index];
      
      if (cell.y > nextCell.y)
      {
        cell.walls.n = false;
        visit(nextCell, "s");
      } else if (cell.x < nextCell.x)
      {
        cell.walls.e = false;
        visit(nextCell, "w");
      } else if (cell.y < nextCell.y)
      {
        cell.walls.s = false;
        visit(nextCell, "n");
      } else if (cell. x > nextCell.x)
      {
        cell.walls.w = false;
        visit(nextCell, "e");
      }

      options = getOptions(cell);
    }
  }

  var startX = Math.floor(Math.random() * width);
  var startY = Math.floor(Math.random() * height);
  visit(cells[startX][startY]);
  
  // Define a space for treasure
  cells[9][9].walls = {e:false,n:false,s:false,w:false};
  cells[10][9].walls = {e:false,n:false,s:false,w:false};
  cells[9][10].walls = {e:false,n:false,s:false,w:false};
  cells[10][10].walls = {e:false,n:false,s:false,w:false};
  cells[8][10].walls.w = false;
  cells[8][10].walls.e = false;
  cells[11][10].walls.w = false;
  cells[11][10].walls.e = false;
  cells[10][8].walls.n = false;
  cells[10][8].walls.s = false;
  cells[10][11].walls.n = false;
  cells[10][11].walls.s = false;

  /* Returns an array of cells that are a path from the cell specified in the
   * first two coordinates to the cell specified in the last two coordinates.
   */
  this.solve = function (startX, startY, endX, endY) 
  {
    startX = startX || 0;
    startY = startY || 0;
    endX = typeof endX == "undefined" ? width - 1 : endX;
    endY = typeof endY == "undefined" ? height - 1 : endY;
    
    // Mark all cells as unvisited:
    for (var x = 0; x < cells.length; x++) 
    {
      for (var y = 0; y < cells[x].length; y++) 
      {
        cells[x][y].visited = false;
      }
    }

    var solution = [];
    var cell = cells[startX][startY];
    var options = [];

    while ((cell.x != endX) || (cell.y != endY))
    {
      cell.visited = true;
      options = getOptions(cell, true);

      if (options.length == 0)
      {
        cell = solution.pop();
      } else {
        solution.push(cell);
        cell = options[0];
      }
    }

    solution.push(cell);

    return solution;
  };
  
  /* A cell in the maze. This cell has some number of walls. */
  function Cell(x, y)
  {
    // The location of the cell:
    this.x = x;
    this.y = y;
    
    // The walls of this cell.
    this.walls = {
        n : true,
        e : true,
        s : true,
        w : true
    };

    // Have we gone through this cell yet?
    this.visited = false;
  }   

  /* Return a table of the complete maze */
  this.draw = function (layer, characters)
  {
    // Returns the actual position of the cell in pixels.
    function actualPosition(cell)
    {
      return [cell.x, cell.y];
    }

    // Initialize variables
    var actualX = 0;
    var actualY = 0;
    var cell;
    
    // Place all sprite walls refering to the generate maze, and add the character to their initial position
    for (var x = 0; x < cells.length; x++) 
    {
      for (var y = 0; y < cells[x].length; y++) 
      {
        cell = cells[x][y];
        actualX = actualPosition(cell)[0] * 2;
        actualY = actualPosition(cell)[1] * 2;

        // Manage all north wally
        if (cell.walls.n)
        {
          if (actualX == cells.length && actualY == 0) 
          {
            
            // Place a player to the north
            for (var nb in characters)
            {
              if (characters[nb].direction == 'N')
              {
                characters[nb].changePosition(actualX + 1, actualY + 1);
              }
            }
          }
          else 
          {
            layer.setGrid('wall', actualX + 1, actualY);
          }
          layer.setGrid('wall', actualX, actualY);
          layer.setGrid('wall', actualX + 2, actualY);
        }
        if (cell.walls.e) 
        {
          if (actualX == (cells.length * 2) - 2 && actualY == cells[x].length) 
          {

            // Place a player to the east
            for (var nb in characters)
            {
              if (characters[nb].direction == 'E')
              {
                characters[nb].changePosition(actualX + 1, actualY + 1);
              }
            }
          }
          else 
          {
            layer.setGrid('wall', actualX + 2, actualY + 1);
          }
          layer.setGrid('wall', actualX + 2, actualY);
          layer.setGrid('wall', actualX + 2, actualY + 2);
        }
        if (cell.walls.s) 
        {
          if (actualX == cells.length && actualY == (cells[x].length * 2) - 2) 
          {

            // Place a player to the north
            for (var nb in characters)
            {
              if (characters[nb].direction == 'S')
              {
                characters[nb].changePosition(actualX + 1, actualY + 1);
              }
            }
          }
          else 
          {
            layer.setGrid('wall', actualX + 1, actualY + 2);
          }
          layer.setGrid('wall', actualX, actualY + 2);
          layer.setGrid('wall', actualX + 2, actualY + 2);
        }
        if (cell.walls.w) 
        {
          if (actualX == 0 && actualY == cells[x].length) 
          {

            // Place a player to the west
            for (var nb in characters)
            {
              if (characters[nb].direction == 'W')
              {
                characters[nb].changePosition(actualX + 1, actualY + 1);
              }
            }
          }
          else 
          {
            layer.setGrid('wall', actualX, actualY + 1);
          }
          layer.setGrid('wall', actualX, actualY);
          layer.setGrid('wall', actualX, actualY + 2);
        }
      }
    }

    /* Draws the solution to the maze; does not draw the maze itself. */
    this.drawSolution = function (positionStart, positionEnd)
    {  
      // Initialize variables
      var tableau = new Array();      
      var positionStart = positionStart || {x: 0, y: 0};
      var positionEnd = positionEnd || {x: (width * 2) - 1, y: (height * 2) - 1};

      // Now draw the solution
      var solution = this.solve(Math.floor(positionStart.x / 2), Math.floor(positionStart.y / 2), Math.floor(positionEnd.x / 2), Math.floor(positionEnd.y / 2));
      for (var i = 0; i < solution.length; i++)
      {
        var position = actualPosition(solution[i]);
        if (i > 0)
        {
          var formerPosition = solution[i - 1];
          if (formerPosition.x < solution[i].x)
          {
            tableau.push({x: (position[0] * 2), y: (position[1] * 2) + 1});
          }
          else if (formerPosition.x > solution[i].x)
          {
            tableau.push({x: (position[0] * 2) + 2, y: (position[1] * 2) + 1});
          }          
          if (formerPosition.y < solution[i].y)
          {
            tableau.push({x: (position[0] * 2) + 1, y: (position[1] * 2)});
          }
          else if (formerPosition.y > solution[i].y)
          {
            tableau.push({x: (position[0] * 2) + 1, y: (position[1] * 2) + 2});
          }          
        }
        tableau.push({x: (position[0] * 2) + 1, y: (position[1] * 2) + 1});
      }
      if (!(tableau[0].x == positionStart.x && tableau[0].y == positionStart.y))
      {
        tableau.unshift(positionStart);
      }
      return tableau;
    }
  };
}

// Server side we set the 'Maze' class to a global type, so that it can use it anywhere.
if( 'undefined' != typeof global ) {
  module.exports = global.Maze = Maze;
}
