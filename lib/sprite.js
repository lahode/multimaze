/**
 * Sprite array class
 */
function Layer(gridSize)
{
  // Table containing the sprites
  this.elements = new Array();

  // Table containing the position of sprites (meta-data)
  this.grid = new Array();
  for (var y = 0; y <= gridSize.height * 2; y++)
  {
    this.grid[y] = new Array();
    for (var x = 0; x <= gridSize.width * 2; x++)
    {
      this.grid[y][x] = 'empty';
    }
  }
  
  this.addElements = function(game)
  {
    for (var y = 0; y <= this.grid.length - 1; y++)
    {
      for (var x = 0; x <= this.grid[y].length - 1; x++)
      {
        switch(this.grid[y][x])
        {
          case 'wall' :
            var image = imageRepository.get('wall');
            this.elements.push(new Sprite('wall', {x: x, y: y}, image, game.config.spriteSize));
            break;
          case 'treasure' :
            this.elements.push(game.treasure);
            break;          
          case 'bonusBox' :
            var bonusBox = game.bonusBox.splice(0, 1);
            this.elements.push(bonusBox[0]);
            break;
          case 'gate' :
            var gates = game.gates.splice(0, 1);
            this.elements.push(gates[0]);
        }
      }
    }
  }

  // Add the new item in the element array only if an object already exists at the same position
  this.push = function(item)
  {
    var name = this.grid[item.position.y][item.position.x];
    if (this.grid[item.position.y][item.position.x] == 'empty')
    {
      item.reset = false;
      // Add the sprite to the sprite elements array
      this.elements.push(item);

      // Update the index to the sprite grid
      if (!(item.position.x in this.grid))
      {
        this.grid[item.position.x] = new Array();
      }
      this.grid[item.position.y][item.position.x] = item.name;
    }
  }

  // Get a sprite from it's position
  this.getFrom = function(x, y)
  {
    for (var index in this.elements)
    {
      if (this.elements[index].position.y == y && this.elements[index].position.x == x)
      {
        // Return the sprite
        return this.elements[index];        
      }
    }
  }

  // Remove an element from the element array
  this.remove = function(index)
  {
    var item = this.elements[index];
    if (item)
    {
      // Update the index to the sprite grid
      this.grid[item.position.y][item.position.x] = 'empty';

      // Remove the sprite from the sprite elements array
      this.elements.splice(index, 1);
    }
  }
  
  // Remove an element by it's position from the element array
  this.removeFrom = function(x, y)
  {
    for (var index in this.elements)
    {
      if (this.elements[index].position.y == y && this.elements[index].position.x == x)
      {
        // Update the index to the sprite grid
        this.grid[y][x] = 'empty';

        // Remove the sprite from the sprite elements array
        this.elements.splice(index, 1);        
      }
    }
  }
  
  this.setGrid = function(name, x, y)
  {
    this.grid[y][x] = name;
  }
}

/**
 * Sprite class
 */
function Sprite(name, position, image, spriteSize)
{
  
  // Initiate sprite object
  this.name = name || 'sprite';
  this.position = position || {x:0, y:0};
  this.zoom = 1;
  this.spriteSize = spriteSize || 32;

  // Update the sprite
  this.update = function() {}

  // Draw a sprite
  this.draw = function(context, scroll, customX, customY)
  {
    // Check if the sprite has an image
    if (image != null)
    {
      // Draw the sprite image
      context.drawImage(
        image,
        0,
        0,
        this.spriteSize,
        this.spriteSize,
        (customX || this.position.x * this.spriteSize) + scroll.x,
        (customY || this.position.y * this.spriteSize) + scroll.y,
        this.spriteSize * this.zoom,
        this.spriteSize * this.zoom
      );
    }
    else
    {
      // Draw a rectangle
      context.fillStyle="#FF00FF";
      context.fillRect(
        (customX || this.position.x * this.spriteSize) + scroll.x,
        (customY || this.position.y * this.spriteSize) + scroll.y,
        this.spriteSize * this.zoom,
        this.spriteSize * this.zoom
      );
    }
  }
}

/**
 * Animated sprite object
 */
function AnimatedSprite(name, position, image, spriteSize, frames, timer, loop, rewind)
{
  // Initiate animated sprite object
  Sprite.call(this, name, position, image, spriteSize);
  this.frames = frames || 24;
  this.frame = 0;
  this.blink = false;
  this.timer = timer || 0;
  this.loop = loop || 0;
  var startAnim = Date.now();
  var startLoop = 0;
  var rewind = rewind || false;
  var alpha = 1;
  var delta = 0.1;

  // Image is mandatory for animated sprite: Check if the image is valid
  try
  {
    var testImg = image.src;
  } catch (e)
  {
    throw 'The image is not valid';
  }
  
  // Update the animation
  this.update = function()
  {
    if (this.timer > 0 && startAnim + this.timer < Date.now())
    {        
      startAnim = Date.now();
      if (this.frame % frames == (frames - 1) && this.loop > 0)
      {
        if (startLoop < this.loop - 1)
        {
          startLoop++;
        }
        else
        {
          this.timer = 0;
        }
      }
      if (this.timer > 0)
      {
        this.frame++;
      }
    }
  }

  // Draw an animated sprite
  this.draw = function(context, scroll, graphPosition)
  {
    var graphPosition = graphPosition || {x: this.position.x * this.spriteSize, y: this.position.y * this.spriteSize};
    
    // Change the alpha for blink effect
    if (this.blink)
    {
      alpha -= delta;
      if (alpha <= 0.3 || alpha >= 1) delta = -delta;
      context.globalAlpha = alpha;
    }
    
    // Draw the image
    context.drawImage(
      image,
      this.spriteSize * (this.frame % frames),
      0,
      this.spriteSize,
      this.spriteSize,
      graphPosition.x + scroll.x,
      graphPosition.y + scroll.y,
        this.spriteSize * this.zoom,
        this.spriteSize * this.zoom
    );
    context.globalAlpha = 1;
  }
}

/**
 * Character animated sprite object
 */
function Character(game, name, position, direction)
{
  // Initiate character object
  var self = this;
  var spriteSize = game.config.spriteSize;
  this.animatedSprites = new Array();
  this.currentSprite = {};
  this.carrySprite = null;
  this.inventory = {};
  this.direction = direction || 'E';
  this.mortal = true;
  this.name = name || 'character';
  this.last_graphPosition = {x: 0, y : 0};
  this.last_carry = '';
  this.carry = '';
  this.position = position;
  this.graphPosition = {x: this.position.x * spriteSize, y: this.position.y * spriteSize};
  this.axion = [];
  this.collision = [];

  // Draw the character
  this.draw = function(canvas, scroll)
  {
    try
    {
      this.currentSprite.draw(canvas, scroll, this.graphPosition);
      if (this.carrySprite)
      {
        this.carrySprite.draw(canvas, scroll, this.graphPosition.x + 15, this.graphPosition.y - 20);
      }
    }
    catch (e)
    {
	    throw ('Error, no animation has been defined!');
    }
  }

  // Change grid position and graphical position according to a new grid position entry
  this.changePosition = function(x, y)
  {
    if (!isNaN(x) && !isNaN(y))
    {
      this.position.x = x;
      this.position.y = y;
    }
    this.graphPosition.x = this.position.x * spriteSize
    this.graphPosition.y = this.position.y * spriteSize;
  }

  // Change grid position and graphical position according to a new graphical position entry
  this.changeGraphPosition = function(x, y)
  {
    if (!isNaN(x) && !isNaN(y))
    {
      this.graphPosition.x = x;
      this.graphPosition.y = y;
    }
    this.position.x = Math.floor((this.graphPosition.x + Math.round(spriteSize / 2)) / spriteSize);
    this.position.y = Math.floor((this.graphPosition.y + Math.round(spriteSize / 2)) / spriteSize);
  }

  // Add a new sprite to the character
  this.addSprite = function(name, image, nbFrames)
  {
    var characterSprite = new AnimatedSprite(name, this.position, image, spriteSize, nbFrames || 24);
    this.animatedSprites[name] = characterSprite;
    this.currentSprite = characterSprite;
  }

  // Check for any updates on the character
  this.update = function()
  {
    if (!this.last_graphPosition) return;

    // Move the character
    if (this.last_graphPosition.x != this.graphPosition.x || this.last_graphPosition.y != this.graphPosition.y)
    {      
      var speed = game.config.speed;
      this.currentSprite = this.animatedSprites[this.direction];
      switch (this.direction)
      {
        case 'E' :
          for(var x = this.last_graphPosition.x; x < this.graphPosition.x; x+=speed)
          {
            this.axion.move(this.direction, true, this.scrollLock, true);
          }
          break;
        case 'W' :
          for(var x = this.last_graphPosition.x; x > this.graphPosition.x; x-=speed)
          {
            this.axion.move(this.direction, true, this.scrollLock, true);
          }
          break;
        case 'S' :
          for(var y = this.last_graphPosition.y; y < this.graphPosition.y; y+=speed)
          {
            this.axion.move(this.direction, true, this.scrollLock, true);
          }
          break;
        case 'N' :
          for(var y = this.last_graphPosition.y; y > this.graphPosition.y; y-=speed)
          {
            this.axion.move(this.direction, true, this.scrollLock, true);
          }
          break;
      }
      this.last_graphPosition.x = this.graphPosition.x;
      this.last_graphPosition.y = this.graphPosition.y;
      this.axion.actionList.waiting.start = 0;
      this.axion.actionList.waiting.lauchAnim = false;
      this.axion.actionList.move.active = false;
    }
    
    // Drop or catch an item
    if (this.last_carry != this.carry)
    {
      this.axion.actionList.dropOrCatch.active = true;
      this.axion.dropOrCatch();
      if (this.axion.actionList.dropOrCatch.active == false)
      {
        this.axion.actionList.waiting.start = 0;
        this.axion.actionList.waiting.lauchAnim = false;
        this.last_carry = this.carry;
      }
    }


    // Execute the character action
    this.axion.waiting();
    /*
    for (var actionName in this.axion.actionList)
    {
      if (this.axion.actionList[actionName].active)
      {
        //console.log(actionName);
        this.axion[actionName]();
        if (actionName != 'waiting')
        {
          this.axion.actionList.waiting.start = 0;
          this.axion.actionList.waiting.lauchAnim = false;
        }
      }
    }
    */
    
    // Set blinking if character is immortal
    this.currentSprite.blink = this.mortal ? false : true;
    
    // Check for collision on the character
    this.collision.check();
  }

  // Kill the sprite
  this.die = function()
  {
    /*
    if (self.mortal)
    {
      alert('Vous êtes mort!');
      throw('Vous êtes mort!');
    }
    */
  }  
}


// Server side we set the 'Maze', 'Sprite', 'AnimatedSprite' and 'Character' to a global type, so that it can use it anywhere.
if( 'undefined' != typeof global ) {
  module.exports = global.Layer = Layer;
  module.exports = global.Character = Character;
}

