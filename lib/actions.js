function Actions(gameEngine, character)
{

  // Initialize the variables
  var self = this;
  this.actionList = {};
  this.actionList.move = {active: false, timer: 20, startLoop: 0, direction: ''};
  this.actionList.dropOrCatch = {active: false, timer: 20, startLoop: 0, frame: ''};
  this.actionList.useInventory = {active: false, frame: ''};
  this.actionList.useBonusstar = {active: false, timer: 3000, startLoop: 0, item: null};
  this.actionList.useBonusbomb = {active: false, timer: 0, explode: false, startLoop: 0, item: null};
  this.actionList.autoMove = {active: false, timer: 20, startLoop: 0, step: 0, index: 0, table: null, search: false, start: null, end: null};
  this.actionList.waiting = {active: true, start: 0, timer: 1000, startLoop: 0, timerLoop: 48, lauchAnim: true};
  this.character = character;
  var game = gameEngine.world;
  var speed = game.config.speed || 4;
  
  // Convert from a starting position to an ending position to direction value
  function startEndConvertDirection(positionStart, positionEnd)
  {
    if (positionStart.x < positionEnd.x)
    {
      return 'E';
    }
    else if (positionStart.x > positionEnd.x)
    {
      return 'W';
    }
    if (positionStart.y < positionEnd.y)
    {
      return 'S';
    }
    else if (positionStart.y > positionEnd.y)
    {
      return 'N';
    }
  }
  
  // This function allow the character to move automatically from one point to another
  this.autoMove = function()
  {
    var direction;
    
    if (!self.actionList.autoMove.start || !self.actionList.autoMove.end)
    {
      return;
    }

    // Loop to make the character immortal for a certain time
    if (self.actionList.autoMove.startLoop == 0)
    {
      // Use the search path if defined
      if (!self.actionList.autoMove.table)
      {
        if (self.actionList.autoMove.search == true)
        {        
          // Get the solution path from a start point to an end point
          self.actionList.autoMove.table = game.maze.drawSolution(self.actionList.autoMove.start, self.actionList.autoMove.end);
        }
        else
        {          
          // Define a one step movement
          self.actionList.autoMove.table = new Array(self.actionList.autoMove.start, self.actionList.autoMove.end);
        }
      }
      
      // Set timer to begin
      self.actionList.autoMove.startLoop = Date.now();
    }
    else if (self.actionList.autoMove.startLoop + self.actionList.autoMove.timer < Date.now())
    {
      // Check if the character have reach a case position or the end of it's path
      if (self.actionList.autoMove.step == 12)
      {
        if (self.actionList.autoMove.index < self.actionList.autoMove.table.length - 2)
        {          
          // Start the next index on the table
          self.actionList.autoMove.index++;
          self.actionList.autoMove.step = 0;
          self.actionList.autoMove.startLoop = Date.now();
        }
        else
        {
          // Finish the action
          self.actionList.autoMove.active = false;
          self.actionList.autoMove.start = null;
          self.actionList.autoMove.end = null;
          self.actionList.autoMove.search = false;
          self.actionList.autoMove.table = null;
          self.actionList.autoMove.startLoop = 0;        
          self.actionList.autoMove.index = 0;
          self.actionList.autoMove.step = 0;
          self.character.changePosition();
        }
      }
      else
      {
        // Get the direction for each step, change the animation accordingly and move forward
        direction = startEndConvertDirection(self.actionList.autoMove.table[self.actionList.autoMove.index], self.actionList.autoMove.table[self.actionList.autoMove.index + 1]);
        self.character.currentSprite = self.character.animatedSprites[direction];
      
        // Move the character
        self.move(direction, true, true);
        self.actionList.autoMove.step++;
        
        // Set timer to restart        
        self.actionList.autoMove.startLoop = Date.now();
      }
    }
  }

  // This function allow the character to move on the maze
  this.move = function(direction, noCollisionCheck, scrollLock, noGraphicalMove)
  {
    var mainLayer = game.mainLayer;
    var scroll = game.scroll;
    var spriteSize = game.config.spriteSize;

    // Convert keyCode to direction value
    self.character.direction = direction || self.actionList.move.direction;

    // Define animation step
    if (self.character.currentSprite)
    {
      self.character.currentSprite.frame += 1;
    }

    // Define the right movement
    switch (self.character.direction)
    {
      case 'N' : // Up move

        // Check for collision
        if (!self.character.collision.check(self.character.direction, noCollisionCheck))
        {
          // Change graphical character position
          if ((noGraphicalMove || false) == false)
          {
            self.character.graphPosition.y -= speed;
          }

          // Scroll the maze if the character is on the edge of the screen
          if ((scrollLock || false) == false && scroll)
          {
            if ((self.character.position.y * spriteSize + scroll.y < 2 * spriteSize) && scroll.y < 0)
            {
              scroll.y += speed;
            }
          }
        }
        break;
      case 'S' : // Down move

        // Check for collision
        if (!self.character.collision.check(self.character.direction, noCollisionCheck))
        {
          // Change graphical character position
          if ((noGraphicalMove || false) == false)
          {
            self.character.graphPosition.y += speed;
          }

          // Scroll the maze if the character is on the edge of the screen
          if ((scrollLock || false) == false && scroll)
          {
            if ((self.character.position.y * spriteSize + scroll.y > game.config.screenSize.height - 4 * spriteSize) && scroll.y > -((maze.height + 1) * spriteSize))
            {
              scroll.y -= speed;
            }
          }
        }
        break;
      case 'W' : // Left move

        // Check for collision
        if (!self.character.collision.check(self.character.direction, noCollisionCheck))
        {
          // Change graphical character position
          if ((noGraphicalMove || false) == false)
          {
            self.character.graphPosition.x -= speed;
          }

          // Scroll the maze if the character is on the edge of the screen
          if ((scrollLock || false) == false && scroll)
          {
            if ((self.character.position.x * spriteSize + scroll.x < 2 * spriteSize) && scroll.x < 0)
            {
              scroll.x += speed;
            }
          }
        }
        break;
      case 'E' : // Right move

        // Check for collision
        if (!self.character.collision.check(self.character.direction, noCollisionCheck))
        {
          // Change graphical character position
          if ((noGraphicalMove || false) == false)
          {
            self.character.graphPosition.x += speed;
          }

          // Scroll the maze if the character is on the edge of the screen
          if ((scrollLock || false) == false && scroll)
          {
            if ((self.character.position.x * spriteSize + scroll.x > game.config.screenSize.width - 4 * spriteSize) && scroll.x > -((maze.width + 1) * spriteSize))
            {
              scroll.x -= speed;
            }
          }
        }
        break;
    }
    
    // Change position on character and meta sprite array
    mainLayer.setGrid('empty', self.character.position.x, self.character.position.y);
    self.character.changeGraphPosition();
    mainLayer.setGrid(self.character.name, self.character.position.x, self.character.position.y);    
  }

  // Draw a path from the player to the end position
  this.solve = function(endPosition)
  {
    var milliseconds = Date.now();
    game.showPath = {startTime: milliseconds, items: game.maze.drawSolution(self.character.position, endPosition)};
  }

  // This function allow the character to drop or catch an object
  this.dropOrCatch = function()
  {
    // Loop the drop Or Catch animation 
    if (self.actionList.dropOrCatch.startLoop == 0)
    {
      self.actionList.dropOrCatch.startLoop = Date.now();
      self.character.currentSprite.frame = self.actionList.dropOrCatch.frame;
      self.character.currentSprite = self.character.animatedSprites['DC'];
    }
    else if (self.actionList.dropOrCatch.startLoop + self.actionList.dropOrCatch.timer < Date.now())
    {
      self.actionList.dropOrCatch.frame++;
      self.actionList.dropOrCatch.startLoop = 0;
    }
    // If animation is finished reset the animation parameters
    else if (self.actionList.dropOrCatch.frame == self.character.currentSprite.frames)
    {
      self.character.currentSprite = 0;
      self.character.currentSprite = self.character.animatedSprites['DC'];
      
      // Reset dropOrCatch
      self.actionList.dropOrCatch.startLoop = 0;
      self.actionList.dropOrCatch.frame = 0;
      self.actionList.dropOrCatch.active = false;

      // Check if the character carry already an item
      if (self.character.carrySprite)
      {
        // Drop the item from his hand
        self.character.carrySprite.position = self.character.position;
        self.character.carrySprite.zoom = 1;
        game.overLayer.push(self.character.carrySprite);
        self.character.carrySprite = null;
      }
      else
      {
        // Catch the item to his hand
        var object = game.overLayer.getFrom(self.character.position.x, self.character.position.y);
        if (object)
        {
          game.overLayer.removeFrom(self.character.position.x, self.character.position.y);
          object.zoom = 0.5;
          self.character.carrySprite = object;          
        }
      }
    }    
  }
  
  // This function allow the character to use the inventory
  this.useInventory = function()
  {

    // Check the existing item
    if (self.character.inventory)
    {
      //self['useBonus' + self.character.inventory.name](self.character.inventory);
      self.actionList['useBonus' + self.character.inventory.name].active = true;
      self.actionList['useBonus' + self.character.inventory.name].item = self.character.inventory;
      document.getElementById('inventory').style.backgroundImage = '';
      self.actionList.useInventory.active = false;
      self.character.inventory = null;
    }
  }
  
  // Use the Bomb from the inventory
  this.useBonusbomb = function()
  {
    // Loop to make the character immortal for a certain time
    if (self.actionList.useBonusbomb.startLoop == 0)
    {
      // Light the bomb
      var bomb = self.actionList.useBonusbomb.item;
      bomb.position = self.character.position;
      game.overLayer.push(bomb);
      self.actionList.useBonusbomb.startLoop = Date.now();
      self.actionList.useBonusbomb.timer = bomb.frames * bomb.timer; 
    }
    else if (self.actionList.useBonusbomb.startLoop + self.actionList.useBonusbomb.timer < Date.now() && !self.actionList.useBonusbomb.explosion)
    {
      // Launch the explosion
      var bomb = self.actionList.useBonusbomb.item;
      self.actionList.useBonusbomb.startLoop = 0;
      game.explosion.position = bomb.position;
      game.overLayer.removeFrom(bomb.position.x, bomb.position.y);
      game.overLayer.push(game.explosion);
      self.actionList.useBonusbomb.startLoop = Date.now();
      self.actionList.useBonusbomb.timer = game.explosion.frames * game.explosion.timer; 
      self.actionList.useBonusbomb.explosion = true;
      self.actionList.useBonusbomb.item = game.explosion;
      
      // Check collision for explosion
      game.explosion.collision.check('N');
      game.explosion.collision.check('W');
      game.explosion.collision.check('S');
      game.explosion.collision.check('E');
      game.explosion.collision.check('');
    }
    else if (self.actionList.useBonusbomb.startLoop + self.actionList.useBonusbomb.timer < Date.now())
    {
      // End the explosion
      var explosion = self.actionList.useBonusbomb.item;
      game.overLayer.removeFrom(explosion.position.x, explosion.position.y);
      self.actionList.useBonusbomb.active = false;
    }
  }

  // Use the star from the inventory
  this.useBonusstar = function()
  {
    // Loop to make the character immortal for a certain time
    if (self.actionList.useBonusstar.startLoop == 0)
    {
      self.actionList.useBonusstar.startLoop = Date.now();
      self.character.mortal = false;
    }
    else if (self.actionList.useBonusstar.startLoop + self.actionList.useBonusstar.timer < Date.now())
    {
      // End the character immortality
      self.actionList.useBonusstar.startLoop = 0;
      self.actionList.useBonusstar.active = false;
      self.character.mortal = true;
    }
  } 

  // Grab the bonus
  this.grabBonus = function()
  {
    var object = game.overLayer.getFrom(self.character.position.x, self.character.position.y);
    if (object)
    {
      game.overLayer.removeFrom(self.character.position.x, self.character.position.y);
    }
    if (!self.character.inventory)
    {
      self.character.inventory = game.bonus[Math.floor(Math.random() * game.bonus.length)];
      var imageTag = imageRepository.get(self.character.inventory.name);
      document.getElementById('inventory').style.backgroundImage = 'url("' + imageTag.src + '")';
    }
  }

  // This function will display a waiting animation
  this.waiting = function()
  {
    // Initiate time count
    if (self.actionList.waiting.start == 0)
    {
      self.actionList.waiting.start = Date.now();
    }
    
    // Check if the animation need to start
    if (self.actionList.waiting.timer > 0 && self.actionList.waiting.start + self.actionList.waiting.timer < Date.now())
    {
      if (!self.actionList.waiting.lauchAnim)
      {
        character.currentSprite = character.animatedSprites[''];
        self.actionList.waiting.lauchAnim = true;
        self.actionList.waiting.startLoop = Date.now();
      }
      else
      {
        if (self.actionList.waiting.startLoop + self.actionList.waiting.timerLoop < Date.now())
        {
          character.currentSprite.frame++;
          self.actionList.waiting.startLoop = Date.now();
        }
      }
    }
  }
}

// Server side we set the 'Actions' class to a global type, so that it can use it anywhere.
if( 'undefined' != typeof global ) {
  module.exports = global.Actions = Actions;
}
