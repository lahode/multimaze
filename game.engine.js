/**
 * The gameEngine class
 */
var gameEngine = function(game_instance)
{
  
  // Store the instance, if any
  this.instance = game_instance;
  
  // Store a flag if we are the server
  this.server = this.instance !== undefined;
  
  // Initialize the default global configuration settings
  this.global_create_configuration();

  // Start a physics loop
  this.create_physics_loop();

  // Start a fast paced timer for measuring time easier
  this.create_timer_loop();

  //Client specific initialisation
  if(!this.server)
  {  
    // Initialize the default global configuration settings
    this.client_initWordGame();
  
    // Create a keyboard handler
    this.keyboard = new THREEx.KeyboardState();

    // Initialize the default client configuration settings
    this.client_create_configuration();

    // Connect to the socket.io server
    this.client_connect_to_server();

    // We start pinging the server to determine latency
    this.client_create_ping_timer();

    // We create a player set
    this.client_players =
    {
      self : new gamePlayer(this),
      others : new Array()
    };
    
    // Initialize the player
    this.client_players.self.init();    
  } 
  else
  {
    // We create a player set
    this.server_players = new Array();
    this.server_time = 0;
    this.laststate = {};    
  }
};

/**
 * Initialize the game world
 */
gameEngine.prototype.client_initWordGame = function() {
  this.world = {};
}

/**
 * Game global configuration
 */
gameEngine.prototype.global_create_configuration = function()
{
  
  this.playerspeed = 120;             // The speed at which the clients move.
  this.capacity = 4;                  // The maximum number of players allowed for this game  
  this.waitingTime = 30;              // Define how much time (in sec.) to wait other player before starting the game
  this.active = false;                // Determine if the game is running
  
  // A local timer for precision on server and client
  this.local_time = 0.016;            // The local timer
  this._dt = Date.now();              // The local timer delta
  this._dte = Date.now();             // The local timer last frame time
  
};//gameEngine.global_create_configuration

/**
 * Game client configuration
 */
gameEngine.prototype.client_create_configuration = function()
{
  this.net_latency = 0.001;           // The latency between the client and the server (ping/2)
  this.net_ping = 0.001;              // The round trip time from here to the server,and back
  this.last_ping_time = 0.001;        // The time we last sent a ping

  this.client_time = 0.01;            //Our local 'clock' based on server time - client interpolation(net_offset).
  this.server_time = 0.01;            //The time the server reported it was at, last we heard from it    
  this.dt = 0.016;                    //The time that the last frame took to run

};//gameEngine.client_create_configuration

/**
 * This loop will manage all the game interaction and will be
 * rendering separately as this happens at a fixed frequency
 */
gameEngine.prototype.create_physics_loop = function() {

    setInterval(function(){
        this._pdt = (new Date().getTime() - this._pdte)/1000.0;
        this._pdte = new Date().getTime();
        this.update_physics();
    }.bind(this), 15);

}; //gameEngine.create_physics_loop

/**
 * Simple timer that update the local time variable
 */
gameEngine.prototype.create_timer_loop = function(){
    setInterval(function(){
        this._dt = new Date().getTime() - this._dte;
        this._dte = new Date().getTime();
        this.local_time += this._dt/1000.0;
    }.bind(this), 4);
} //gameEngine.create_timer_loop

/**
 * Ping the server to determine it's latency
 */
gameEngine.prototype.client_create_ping_timer = function()
{
  //Set a ping timer to 1 second, to maintain the ping/latency between
  //client and server and calculated roughly how our connection is doing
  setInterval(function()
  {
    this.last_ping_time = Date.now();
    this.socket.send('p.' + (this.last_ping_time));
  }.bind(this), 1000);
    
}; //gameEngine.client_create_ping_timer

/**
 * Handle connection to the socket.io server and add listeners to the client
 */
gameEngine.prototype.client_connect_to_server = function() 
{      
  //Store a local reference to our connection to the server
  this.socket = io.connect();

  //When we connect, we are not 'connected' until we have a server id
  //and are placed in a game by the server. The server sends us a message for that.
  this.socket.on('connect', function()
  {
    this.client_players.self.state = 'connecting';
  }.bind(this));

  // Sent when we are disconnected (network, server down, etc)
  this.socket.on('disconnect', this.client_ondisconnect.bind(this));
  
  // Sent each tick of the server simulation. This is our authoritive update
  this.socket.on('onserverupdate', this.client_onserverupdate_received.bind(this));
  
  // Handle when we connect to the server, showing state and storing id's.
  this.socket.on('onconnected', this.client_onconnected.bind(this));
  
  // On error we just show that we are not connected for now. Can print the data.
  this.socket.on('error', this.client_ondisconnect.bind(this));
  
  // On message from the server, we parse the commands and send it to the handlers
  this.socket.on('message', this.client_onnetmessage.bind(this));

}; //gameEngine.client_connect_to_server

/**
 * The server responded that we are now in a game,
 * this lets us store the information about ourselve
 */
gameEngine.prototype.client_onconnected = function(data) {

    this.client_players.self.id = data.id;
    this.client_players.self.state = 'connected';
    this.client_players.self.online = true;

}; //gameEngine.client_onconnected

/**
 * When we disconnect, we don't know if the other player is
 * connected or not, and since we aren't, everything goes to offline
 */
gameEngine.prototype.client_ondisconnect = function(data)
{
  // Update player status
  this.client_players.self.state = 'not-connected';
  this.client_players.self.online = false;
  
  // Stop the game updates immediate
  this.stop_update();

}; //gameEngine.client_ondisconnect

/**
 * Update the game on the client side
 */
gameEngine.prototype.client_onserverupdate_received = function(data)
{
  for (var uid in data.players)
  {
    if (this.client_players.self.id == uid)
    {
      for (var command in data.players[uid])
      {
        this.client_players.self[command] = data.players[uid][command];
      }
    }
    else
    {
      for (var command in data.players[uid])
      {
        this.client_players.others[uid][command] = data.players[uid][command];
      }
    }
  }
}; //gameEngine.client_onserverupdate_received

/**
 * Execute an incoming message from the client
 */
gameEngine.prototype.client_onnetmessage = function(data)
{
    var commands = data.split('.');
    var command = commands[0];
    var subcommand = commands[1] || null;
    var commanddata = commands[2] || null;
    var subcommanddata = commands[3] || null;

    switch(command) {
        case 's': //server message

            switch(subcommand) {

                case 'h' : // host a game requested
                    this.client_onhostgame(commanddata); break;

                case 'j' : // join a game requested
                    this.client_onjoingame(commanddata); break;

                case 'r' : // ready a game requested
                    this.client_onreadygame(commanddata); break;

                case 'e' : // end game requested
                    this.client_ondisconnect(commanddata); break;

                case 'p' : // server ping
                    this.client_onping(commanddata); break;

                case 'i' : // Show available instances
                    this.client_showinstances(commanddata);break;

                case 'm' : // show message
                    this.client_showmessage(commanddata, subcommanddata); break;
            } //subcommand

        break; //'s'
    } //command
                
}; //gameEngine.client_onnetmessage

/**
 * Update player information when hosting a new game
 */
gameEngine.prototype.client_onhostgame = function(data) {

    // The server sends the time when asking us to host, but it should be a new game.
    // so the value will be really small anyway (15 or 16ms)
    var server_time = parseFloat(data.replace('-','.'));

    // Get an estimate of the current time on the server
    this.local_time = server_time + this.net_latency;

    // Set the flag that we are hosting, this helps us position respawns correctly
    this.client_players.self.host = true;

    // Update debugging information to display state
    this.client_players.self.state = 'hosting.waiting for a player';

}; //gameEngine.client_onhostgame

/**
 * Update player information when joining a game
 */
gameEngine.prototype.client_onjoingame = function(data) {
  
    // We are not the host
    this.client_players.self.host = false;
    
    // Update the local state
    this.client_players.self.state = 'connected.joined.waiting';

}; //gameEngine.client_onjoingame

/**
 * Updating all players information before starting the game
 */
gameEngine.prototype.client_onreadygame = function(data) {

  var datas = JSON.parse(data);
  var server_time = parseFloat(datas.local_time.replace('-','.'));

  this.local_time = server_time + this.net_latency;
  console.log('server time is about ' + this.local_time);
  
  // Update our status
  this.client_players.self.state  = this.client_players.self.host ? 'local_pos(hosting)' : 'local_pos(joined)';

  // Add all other players and update their information
  for (var p in datas.players)
  {
    if (datas.players[p].id != this.client_players.self.id)
    {
      var other_player = new gamePlayer(this);
      other_player.init();
      other_player.id = datas.players[p].id;
      other_player.host = datas.players[p].host;
      other_player.state = datas.players[p].host ? 'local_pos(hosting)' : 'local_pos(joined)';
      other_player.online = true;           
      this.client_players.others[other_player.id] = other_player;
    }      
  }
  
  // Set this flag, so that the update loop can run it.
  this.active = true;
  
  // Tell the client that the game has started
  this.client_showmessage('starting');
  
  // Start the loop
  this.update(new Date().getTime());

}; //gameEngine.client_onreadygame

/**
 * Update the ping information from the server
 */
gameEngine.prototype.client_onping = function(data) {

    this.net_ping = Date.now() - parseFloat( data );
    this.net_latency = this.net_ping / 2;

}; //gameEngine.client_onping

/**
 * Show available instances
 */
gameEngine.prototype.client_showinstances = function(instances)
{
  for(var i in instances)
  {
    console.log('game ' + instances[i] + ' available');
  }
} //gameEngine.client_showinstances

/**
 * Display a message coming from the server
 */
gameEngine.prototype.client_showmessage = function(type, text)
{
  console.log(type + ' : ' + text);
} //gameEngine.client_showmessage

/**
 * Main update loop
 */
gameEngine.prototype.update = function(t)
{    
  // Work out the delta time
  this.dt = this.lastframetime ? ((t - this.lastframetime)/1000.0).fixed() : 0.016;

  // Store the last frame time
  this.lastframetime = t;

  // Update the game specifics
  if (!this.server)
  {
    this.client_update();
  }
  else
  {
    this.server_update();
  }

  // Schedule the next update
  this.updateid = window.requestAnimationFrame(this.update.bind(this), this.viewport);

}; //gameEngine.update

/**
 * Update the physics in the game
 */
gameEngine.prototype.update_physics = function() {

    if(this.server) {
        this.server_update_physics();
    } else {
        this.client_update_physics();
    }

}; //gameEngine.prototype.update_physics

/**
 * Handle all the game physics on the server side
 *
 * -- To be redefined --
*/
gameEngine.prototype.server_update_physics = function() {
  // Process
  for (var p in this.server_players)
  {
    this.process_input(this.server_players[p]);
  }
} //gameEngine.prototype.server_update_physics

/**
 * Handle all the game physics on the client side
 *
 * -- To be redefined --
 */
gameEngine.prototype.client_update_physics = function() {
} //gameEngine.prototype.client_update_physics

/**
 * Stop the update loop
 */
gameEngine.prototype.stop_update = function()
{
  window.cancelAnimationFrame(this.updateid);
}; //gameEngine.stop_update

/**
 * Update on the server side
 *
 * Makes sure things run smoothly and 
 * notifies clients of changes
 */
gameEngine.prototype.server_update = function()
{
  // Update the state of our local clock to match the timer
  this.server_time = this.local_time;
  this.laststate = {t : this.server_time, players : {}};
  var players = this.server_players;

  // Make a snapshot of the current state, for updating the clients
  for (var uid in players)
  {
    this.laststate.players[uid] = {
      position : players[uid].position,                 // player position
      last_input_seq : players[uid].last_input_seq      // player last inputs sequence
    };
  }
  
  // Send the snapshot to the all players
  for (var uid in players)
  {
    this.server_players[uid].instance.emit('onserverupdate', this.laststate);
  }

}; //gameEngine.server_update

/**
 * Update on the client side
 *
 * -- To be redefined --
 */
gameEngine.prototype.client_update = function()
{
  // Clean the canvas
  context.clearRect(0, 0, 500, 500);
  
  // Capture inputs from the player
  this.client_handle_input();

  // Draw the game
  
} //gameEngine.client_update

/**
 * Start the game
 *
 * -- To be redefined --
 */
gameEngine.prototype.client_create_join_game = function(gameID)
{
  var command = gameID ? 'j.' : 's.';
  this.socket.send(
    command + gameID + '.' + JSON.stringify({
      id: this.client_players.self.id,
      name: this.client_players.self.name,
      position: this.client_players.self.position,
    }) + '.' + JSON.stringify({})
  );
} //gameEngine.createNewGame

/**
 * Handle input on the client side
 *
 * -- To be redefined --
 */
gameEngine.prototype.client_handle_input = function(newInput)
{
  // Initialize variables
  var input = [];

  // Store the inputs and send it to the server
  if(input.length) {

    // Update what sequence we are on now
    this.input_seq += 1;

    // Store the input state as a snapshot of what happened.
    this.client_players.self.inputs.push({
        inputs : input,
        time : this.local_time.fixed(3),
        seq : this.input_seq
    });

    // Send the packet of information to the server.
    // The input packets are labelled with an 'i' in front.
    var server_packet = 'i.';
        server_packet += input.join('-') + '.';
        server_packet += this.local_time.toFixed(3).replace('.','-') + '.';
        server_packet += this.input_seq;

    // Send the packet to the server
    this.socket.send(server_packet);
  }
}; //gameEngine.client_handle_input

/**
 * Process the input on the player (can be both, client & server side)
 *
 * -- To be redefined --
 */
gameEngine.prototype.process_input = function( player ) {
  var result;

  // Check if there are inputs to process
  if (player.inputs.length)
  {
    for(var input in player.inputs) 
    {
      switch(input)
      {
        default: break;
      }
    }

    // We can now clear the array since these have been processed
    while(player.inputs.length > 0) {player.inputs.pop();}
  }

} //gameEngine.process_input

/**
 * Add new player when player is joining
 *
 * -- To be redefined -- 
 */
gameEngine.prototype.server_add_new_player = function(playerInfo) {
  var playerInfo = playerInfo ? JSON.parse(playerInfo) : [];
  var player = new gamePlayer(this, this.instance.players[playerInfo.id]);
  player.id = playerInfo.id;
  player.name = playerInfo.name;
  player.position = playerInfo.position;
  this.server_players[player.id] = player;
} //gameEngine.server_add_new_player

/**
 * Configure world with host player configuration
 *
 * -- To be redefined -- 
 */
gameEngine.prototype.server_initWordGame = function(gameInfo) {
  var gameInfo = gameInfo ? JSON.parse(gameInfo) : [];
  this.world = [];
} //gameEngine.server_initWordGame

/**
 * Initiate the player
 */
var gamePlayer = function (game_instance, player_instance)
{ 
  // Store the instance, if any
  this.instance = player_instance;
  this.game = game_instance;

  // Set up initial values for our state information
  this.name = '';
  this.position = {x: 0, y: 0};
  this.state = 'not-connected';
  this.id = '';

  // These are used in moving us around later
  /*
  this.old_state = {pos:{x:0,y:0}};
  this.cur_state = {pos:{x:0,y:0}};
  this.state_time = Date.now();
  */

  // Our local history of keyboard inputs
  this.inputs = [];
  this.last_input_seq = 0;

}; //gamePlayer.constructor

gamePlayer.prototype.init = function()
{
}

/**
 * (4.22208334636).fixed(n) will return value with only n decimals, default n = 3
 */
Number.prototype.fixed = function(n)
{
  n = n || 3;
  return parseFloat(this.toFixed(n));
};

/**
 * The main update loop runs on requestAnimationFrame,
 * Which falls back to a setTimeout loop on the server
 * Code below is from Three.js, and sourced from links below
 *
 *  http://paulirish.com/2011/requestanimationframe-for-smart-animating/
 *  http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
 *
 *  requestAnimationFrame polyfill by Erik M�ller
 *  fixes from Paul Irish and Tino Zijdel
 */
var frame_time = 60/1000; // run the local game at 16ms/ 60hz
if('undefined' != typeof(global)) frame_time = 45; //on server we run at 45ms, 22hz

( function () {

    var lastTime = 0;
    var vendors = [ 'ms', 'moz', 'webkit', 'o' ];

    for ( var x = 0; x < vendors.length && !window.requestAnimationFrame; ++ x ) {
        window.requestAnimationFrame = window[ vendors[ x ] + 'RequestAnimationFrame' ];
        window.cancelAnimationFrame = window[ vendors[ x ] + 'CancelAnimationFrame' ] || window[ vendors[ x ] + 'CancelRequestAnimationFrame' ];
    }

    if ( !window.requestAnimationFrame ) {
        window.requestAnimationFrame = function ( callback, element ) {
            var currTime = Date.now(), timeToCall = Math.max( 0, frame_time - ( currTime - lastTime ) );
            var id = window.setTimeout( function() { callback( currTime + timeToCall ); }, timeToCall );
            lastTime = currTime + timeToCall;
            return id;
        };
    }

    if ( !window.cancelAnimationFrame ) {
        window.cancelAnimationFrame = function ( id ) { clearTimeout( id ); };
    }

}());

// Server side we set the 'gameEngine' class to a global type, so that it can use it anywhere.
if( 'undefined' != typeof global ) {
    module.exports = global.gameEngine = gameEngine;
    module.exports = global.gamePlayer = gamePlayer;
}

// Get game information
require('./game.js');
