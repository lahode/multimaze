/*  Copyright (c) 2012 Sven "FuzzYspo0N" Bergström
    
    written by : http://underscorediscovery.com
    written for : http://buildnewgames.com/real-time-multiplayer/
    
    MIT Licensed.
*/

// Initialize variables
var game_server = module.exports = { games : {}, game_count:0, game_open:0 };
var UUID        = require('node-uuid');
var verbose     = true;

game_server.fake_latency = 0; // Value to simulate a latency on the server
game_server.local_time = 0; // Initiate time in seconds since the server has started
game_server._dt = Date.now(); // The time that the last frame took to run on the server (in millisecond)
game_server._dte = Date.now(); // Starting date from the server
game_server.messages = []; // A local queue of messages we delay if faking latency

// Since we are sharing code with the browser, we are going to include some values to handle that.
global.window = global.document = global;

// Import shared game library code.
require('./game.engine.js');

/**
 * A simple wrapper for logging so we can toggle it, and augment it for clarity.
 */
game_server.log = function()
{
  if(verbose) console.log.apply(this,arguments);
};

/**
 * Count the time on the server and determine the DT
 */
setInterval (function()
{
  game_server._dt = Date.now() - game_server._dte;
  game_server._dte = Date.now();
  game_server.local_time += game_server._dt / 1000.0;
}, 4);

/**
 * Store all message incoming message from the client and
 * treat each of them.
 */
game_server.onMessage = function(client, message)
{
  if (this.fake_latency && message.split('.')[0].substr(0,1) == 'i')
  {
    // Store all input message
    game_server.messages.push({client:client, message:message});

    setTimeout(function(){
      if(game_server.messages.length) {
        game_server._onMessage( game_server.messages[0].client, game_server.messages[0].message );
        game_server.messages.splice(0,1);
      }
    }.bind(this), this.fake_latency);
  }
  else
  {
    game_server._onMessage(client, message);
  }
};

/**
 * Execute an incoming message from the client
 */
game_server._onMessage = function(client, message) {

  //Cut the message up into sub components
  var message_parts = message.split('.');
  
  //The first is always the type of message
  var message_type = message_parts[0];
  
  // If client.game does not exist, then the game hasn't started yet
  if (!('game' in client))
  {
    // Send all existing game instances to the client so he can join one
    var totalGameInstance = new Array();
    for(var gameid in this.games)
    {
      if (this.games[gameid].gamecore.capacity > this.games[gameid].player_count && this.games[gameid].status == 'waiting')
      {
        totalGameInstance.push(gameid);
      }
    }
    client.send('s.i.' + JSON.stringify(totalGameInstance))

    // Start a new game
    switch(message_type)
    {
      case 's' :
        this.createGame(client, message_parts[2], message_parts[3]);
        break;
      case 'j' :
        this.joinGame(client, message_parts[1], message_parts[2]);
        break;
    }
    return;
  }
  else if (this.game_open > 0)
  {
    // Wait until the game is full or the waiting time is over
    for(var gameid in this.games)
    {
      // Check if game status is waiting
      if (this.games[gameid].status == 'waiting')
      {
        // Start the game if it is full or has finished waiting
        if (this.games[gameid].gamecore.capacity == this.games[gameid].player_count ||
           (this.games[gameid].initTime + (this.games[gameid].gamecore.waitingTime * 1000) < Date.now()))
        {
          // Start running the game on the server
          this.startGame(this.games[gameid]);
          
          // Reduce the number of opened game by one
          this.game_open--;
          return;
        }
      
        // Log the time down until start
        var remainingWaitingTime = this.games[gameid].gamecore.waitingTime - Math.round((Date.now() - this.games[gameid].initTime) / 1000);
        this.log('Game will start in ' + remainingWaitingTime + ' s.');

        // Send a message to the client
        client.send('s.m.waiting.' + remainingWaitingTime);
      }
    }
  }

  // Handle other message types
  switch(message_type)
  {
    case 'i' : 
      // Input handler will forward this
      this.onInput(client, message_parts);
      break;
    case 'p' : 
      // Send a ping to the client
      client.send('s.p.' + message_parts[1]);
      break;
    case 'l' :
      // A client is asking for lag simulation
      this.fake_latency = parseFloat(message_parts[1]);
      break;
  }
}; //game_server.onMessage

/**
 * Manage the keyboard inputs from the client during the game
 */
game_server.onInput = function(client, parts)
{
  // The input commands come in like u-l,
  // so we split them up into separate commands,
  // and then update the players
  var input_commands = parts[1].split('-');
  var input_time = parts[2].replace('-','.');
  var input_seq = parts[3];

  // The client should be in a game, so
  // we can tell that game to handle the input
  if(client && client.game && client.game.gamecore)
  {
    client.game.gamecore.server_players[client.userid].inputs.push({inputs:input_commands, time:input_time, seq:input_seq});
  }

}; //game_server.onInput

/**
 * Create a new game instance
 */
game_server.createGame = function(player, playerInfo, gameInfo)
{
  // Create a new game instance
  var thegame =
  {
    id : UUID(),                //generate a new id for the game
    player_host:player,         //so we know who initiated the game
    players: [],                // Players connected
    player_count:1,             //for simple checking of state
    status: 'waiting',          //Define by default a waiting statut for the new game
    initTime: Date.now()        //Game instance creating time
  };

  // Store it in the list of game
  this.games[thegame.id] = thegame;

  // Keep track
  this.game_count++;
  
  // Add one new open game
  this.game_open++;

  // Create a new game core instance and set client host's game configuration.
  thegame.gamecore = new gameEngine(thegame);
  thegame.gamecore.server_initWordGame(gameInfo);
  
  // Add player to the game and set client host's player configuration.
  thegame.players[player.userid] = player;
  thegame.gamecore.server_add_new_player(playerInfo);
  
  // Start updating the game loop on the server
  thegame.gamecore.update(Date.now());

  // Tell the player that they are now the host (s = server message, h = you are hosting)
  player.send('s.h.'+ String(thegame.gamecore.local_time).replace('.','-'));
  player.game = thegame;
  player.hosting = true;
  
  // Log message
  this.log('server host at ' + thegame.gamecore.local_time + 's.');
  this.log('player ' + player.userid + ' created a game with id ' + player.game.id);

  //return it
  return thegame;

}; //game_server.createGame

/**
 * Join an existing game instance
 */
game_server.joinGame = function(player, gameID, playerInfo)
{
  try
  {
    var thegame = this.games[gameID];
    
    // Check if game is already full
    if (thegame.gamecore.capacity <= thegame.player_count)
    {
      this.log('The game ' + gameID + ' is already full');
      return;
    }

  // Add player to the game and set client's player configuration.
    thegame.players[player.userid] = player;
    thegame.gamecore.server_add_new_player(playerInfo);
    
    // Keep track
    thegame.player_count++;
    
    // Tell the player that they are now joining an instance (s = server message, j = you are joining)
    player.send('s.j');
    player.game = thegame;
    player.hosting = false;

    // Log message
    this.log('player ' + player.userid + ' joined the game with id ' + player.game.id);       
  } catch (e)
  {
    // Display an error
    this.log('Error, cannot attach player: ' + player.userid + ' to game: ' + gameID);
  }
}

/**
 * Start the game.
 */
game_server.startGame = function(game) {

  // Tell all the client that the game is ready to start and send all players ID's
  var players_info = new Array();
  for (var uid in game.players)
  {
    players_info.push({id: uid, host: game.players[uid].hosting});
  }
  
  // Tell all the client that the game is ready to start and send all players ID's
  for (var uid in game.players)
  {
    // Tell the client to start the game
    game.players[uid].send('s.r.' + JSON.stringify({local_time: String(game.gamecore.local_time).replace('.','-'), players: players_info,
    world : {overLayer: game.gamecore.world.overLayer, mainLayer: game.gamecore.world.mainLayer}}));
  }

  // Set this flag, so that the update loop can run it.
  game.active = true;
  game.status = 'running';

}; //game_server.startGame

/**
 * Manage a end of game or disconnections.
 */
game_server.endGame = function(gameid, userid) 
{
  var thegame = this.games[gameid];
  
  // Check if the game exists
  if(thegame) 
  {
    // Check if the game has still multiple players before the player left
    if (thegame.player_count > 1)
    {      
      // Check if disconnected player isn't hosting
      if (thegame.players[userid].hosting == false)
      {
        // Tell all the connected client that the player left the game
        for (var p in thegame.players)
        {
          thegame.players[p].send('s.m.disconnected.' + userid);
        }

        // Keep track
        thegame.player_count++;

        // Continue playing without the player
        return;
      }
      
      // Tell all the connected client the game is ended
      for (var p in thegame.players)
      {
        thegame.players[p].send('s.e');
      }
    }
    else {
      thegame.player_host.send('s.e')
    }
    
    // Stop the game updates immediate
    thegame.gamecore.stop_update();

    // Remove the game
    delete this.games[gameid];
    this.game_count--;
    this.log('game removed. there are now ' + this.game_count + ' games');
  } 
  else
  {
    this.log('that game was not found!');
  }

}; //game_server.endGame
