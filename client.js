// A window global for our game root variable.
var mainGame = {};

// When loading, we store references to our
// drawing canvases, and initiate a game instance.
window.onload = function()
{
  // Create our game client instance.
  mainGame = new gameEngine();
  
  // Fetch the viewport
  mainGame.viewport = document.getElementById('maze');
  
  // Adjust its size
  mainGame.viewport.width = mainGame.world.config.screenSize.width;
  mainGame.viewport.height = mainGame.world.config.screenSize.height;

  // Fetch the rendering contexts
  mainGame.ctx = mainGame.viewport.getContext('2d');        

  // Attach click handler on "Start new instance" button
  document.getElementById('start').addEventListener('click', function() {
    
    // Start the loop
    mainGame.client_create_join_game();
  });
}

var require = function() {}

// Message before leaving
/*
window.onbeforeunload = function(){
  return "Vous allez quitter Multimaze";
};
*/