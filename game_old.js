/**
 * Define an object to hold all our images for the game so images
 * are only ever created once. This type of object is known as a
 * singleton.
 */
var imageRepository = new function()
{
	// Initialize the variables
	var numImages = 0;
	var numLoaded = 0;
  var self = this;
  this.start = false;

	// Define images
  this.images = new Array();
  
  // Add new image
  this.add = function(name, source)
  {

    // Check if an image already exist with the same name
    if (name in this.images)
    {
      throw ('Image already exist for ' + name);
    }
    
    // Initialize the new image and add it to the image array
    var newImage = new Image();
    newImage.src = source;
    newImage.onload = function()
    {
      imageLoaded();
    }
    this.images[name] = newImage;
    numImages++;
  }
  
  // Get image
  this.get = function(name) 
  {
    return this.images[name];
  }
  
	// Ensure all images have loaded before starting the game
  function imageLoaded()
  {
		numLoaded++;
		if (numLoaded === numImages && self.start) 
    {
      game.play = true;
      animate();
		}
	}
}


/**
 * requestAnim shim layer by Paul Irish
 * Finds the first API that works to optimize the animation loop,
 * otherwise defaults to setTimeout().
 */
window.requestAnimFrame = (function()
{
	return  window.requestAnimationFrame   ||
			window.webkitRequestAnimationFrame ||
			window.mozRequestAnimationFrame    ||
			window.oRequestAnimationFrame      ||
			window.msRequestAnimationFrame     ||
			function(callback, element)
      {
				window.setTimeout(callback, 1000 / 60);
			};
})();


/**
 * The animation loop. Calls the requestAnimationFrame shim to
 * optimize the game loop and draws all game objects. This
 * function must be a gobal function and cannot be within an
 * object.
 */
function animate() 
{
  game.draw();
  if (game.play)
  {
    requestAnimFrame(animate);
    switch (game.gameAnimation.state)
    {
      case 'start' : 
        game.startAnimation();
        break;
      case 'finish' :
        game.finishAnimation();
        break;
    }
  }
}

/**
 * Game configuration class
 */
function GameConfiguration()
{
  this.spriteSize = 48;
  this.screenSize = {width: 640, height: 640};
  this.mazeSize = {width: 20, height: 20};
  this.speed = 4;
  this.bonusBoxNumber = 10;
}

/**
 * Array shuffle
 */
function shuffleArray (array) {
  var i = 0;
  var j = 0;
  var temp = null;

  for (i = array.length - 1; i > 0; i -= 1)
  {
    j = Math.floor(Math.random() * (i + 1));
    temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }
}  

/**
 * Game class
 */
function Game(configuration)
{

  // Initialize properties
  this.spriteID = 0;
  this.gameRun = true;
  this.config = configuration;
  this.mainLayer = new Layer(this.config.mazeSize);
  this.overLayer = new Layer(this.config.mazeSize);
  this.maze = new Maze(this.config.mazeSize.width, this.config.mazeSize.height);
  this.players = new Array();
  this.debugSprite;
  this.scroll;
  this.explosion;
  this.bonusBox = new Array();
  this.bonus = new Array();
  this.gates = new Array();
  this.play = false;
  this.enableAction = false;
  this.gameAnimation = {state: 'start', winner: {}, startTimerPlayer: 0, startTimerGate: 0, timerPlayer: 0, timerGate: 0}

  
  // Initialize internal variables
  var self = this;
  var canvas = document.getElementById("maze");
  var context = canvas.getContext("2d");
  var spriteSize = this.config.spriteSize;
  var showPath;
  this.cont = context;
  
  /**
   * Function to initialize the maze
   */
  this.init = function()
  {
    // Initialize variables
    var debug = false;
    var NESW = ["N", "E", "S", "W"];
    shuffleArray(NESW);

    // Initiate scroll
    switch(NESW[0])
    {
      case 'W' : this.scroll = {x: 0 * spriteSize, y: -15 * spriteSize};break;
      case 'E' : this.scroll = {x: -28 * spriteSize, y: -15 * spriteSize};break;
      case 'S' : this.scroll = {x: -15 * spriteSize, y: -28 * spriteSize};break;
      case 'N' : this.scroll = {x: -15 * spriteSize, y: 0};break;
    }

    // Set the canvas size
    canvas.width = this.config.screenSize.width;
    canvas.height = this.config.screenSize.height;

    // Initialize the debugger
    this.debugSprite = new Sprite('debug', {x: 0, y: 0}, spriteSize);

    // Initialize player n°1
    imageRepository.add('character1_moveRight', 'images/GentillePoops_Poopy_RunRightCycle.png');
    imageRepository.add('character1_moveLeft', 'images/GentillePoops_Poopy_RunLeftCycle.png');
    imageRepository.add('character1_moveUp', 'images/GentillePoops_Poopy_RunBackCycle.png');
    imageRepository.add('character1_moveDown', 'images/GentillePoops_Poopy_RunFrontCycle.png');
    imageRepository.add('character1_dropOrCatch', 'images/GentillePoops_Poopy_DropThing.png');
    imageRepository.add('character1_waiting', 'images/GentillePoops_Poopy_Waiting.png');
    this.players[0] = new CharacterSprite('player1', {x: 0, y: 0}, NESW[0]);
    this.players[0].addSprite('DC', imageRepository.get('character1_dropOrCatch'));
    this.players[0].addSprite('E', imageRepository.get('character1_moveRight'));
    this.players[0].addSprite('N', imageRepository.get('character1_moveUp'));
    this.players[0].addSprite('S', imageRepository.get('character1_moveDown'));
    this.players[0].addSprite('W', imageRepository.get('character1_moveLeft'));
    this.players[0].addSprite('', imageRepository.get('character1_waiting'));
 
    // Initialize player n°1
    imageRepository.add('character2_moveRight', 'images/VilainePoops_Poopy_RunRightCycle.png');
    imageRepository.add('character2_moveLeft', 'images/VilainePoops_Poopy_RunLeftCycle.png');
    imageRepository.add('character2_moveUp', 'images/VilainePoops_Poopy_RunBackCycle.png');
    imageRepository.add('character2_moveDown', 'images/VilainePoops_Poopy_RunFrontCycle.png');
    imageRepository.add('character2_dropOrCatch', 'images/VilainePoops_Poopy_DropThing.png');
    imageRepository.add('character2_waiting', 'images/VilainePoops_Poopy_Waiting.png');
    this.players[1] = new CharacterSprite('player2', {x: 1, y: 11}, NESW[1]);
    this.players[1].addSprite('DC', imageRepository.get('character2_dropOrCatch'));
    this.players[1].addSprite('E', imageRepository.get('character2_moveRight'));
    this.players[1].addSprite('N', imageRepository.get('character2_moveUp'));
    this.players[1].addSprite('S', imageRepository.get('character2_moveDown'));
    this.players[1].addSprite('W', imageRepository.get('character2_moveLeft'));
    this.players[1].addSprite('', imageRepository.get('character2_waiting'));

    // Initialize player n°3
    imageRepository.add('character3_moveRight', 'images/Ninja_Poopy_RunRightCycle.png');
    imageRepository.add('character3_moveLeft', 'images/Ninja_Poopy_RunLeftCycle.png');
    imageRepository.add('character3_moveUp', 'images/Ninja_Poopy_RunBackCycle.png');
    imageRepository.add('character3_moveDown', 'images/Ninja_Poopy_RunFrontCycle.png');
    imageRepository.add('character3_dropOrCatch', 'images/Ninja_Poopy_DropThing.png');
    imageRepository.add('character3_waiting', 'images/Ninja_Poopy_Waiting.png');
    this.players[2] = new CharacterSprite('player3', {x: 1, y: 31}, NESW[2]);
    this.players[2].addSprite('DC', imageRepository.get('character3_dropOrCatch'));
    this.players[2].addSprite('E', imageRepository.get('character3_moveRight'));
    this.players[2].addSprite('N', imageRepository.get('character3_moveUp'));
    this.players[2].addSprite('S', imageRepository.get('character3_moveDown'));
    this.players[2].addSprite('W', imageRepository.get('character3_moveLeft'));
    this.players[2].addSprite('', imageRepository.get('character3_waiting'));

    // Initialize player n°4
    imageRepository.add('character4_moveRight', 'images/Franky_Poopy_RunRightCycle.png');
    imageRepository.add('character4_moveLeft', 'images/Franky_Poopy_RunLeftCycle.png');
    imageRepository.add('character4_moveUp', 'images/Franky_Poopy_RunBackCycle.png');
    imageRepository.add('character4_moveDown', 'images/Franky_Poopy_RunFrontCycle.png');
    imageRepository.add('character4_dropOrCatch', 'images/Franky_Poopy_DropThing.png');
    imageRepository.add('character4_waiting', 'images/Franky_Poopy_Waiting.png');
    this.players[3] = new CharacterSprite('player4', {x: 39, y: 11}, NESW[3]);
    this.players[3].addSprite('DC', imageRepository.get('character4_dropOrCatch'));
    this.players[3].addSprite('E', imageRepository.get('character4_moveRight'));
    this.players[3].addSprite('N', imageRepository.get('character4_moveUp'));
    this.players[3].addSprite('S', imageRepository.get('character4_moveDown'));
    this.players[3].addSprite('W', imageRepository.get('character4_moveLeft'));
    this.players[3].addSprite('', imageRepository.get('character4_waiting'));

    // Initialize the maze
    imageRepository.add('wall', 'images/wall48.png');
    this.maze.draw(imageRepository.get('wall'));
    this.mainLayer.push(this.players[0]);
    this.mainLayer.push(this.players[1]);
    this.mainLayer.push(this.players[2]);
    this.mainLayer.push(this.players[3]);
    
    // Add treasure
    imageRepository.add('treasure', 'images/treasure48.png');
    this.treasure = new Sprite('treasure', {x: game.config.mazeSize.width, y: game.config.mazeSize.height}, imageRepository.get('treasure'));
    this.overLayer.push(this.treasure);
    
    // Add bonus box
    imageRepository.add('bonusBox', 'images/bonus48.png');
    var testX;
    var testY;
    for (var i=0; i<this.config.bonusBoxNumber;i++)
    {
      do {
        testX = Math.floor((Math.random() * (2 * this.config.mazeSize.width - 2)) + 1);
        testY = Math.floor((Math.random() * (2 * this.config.mazeSize.height - 2)) + 1);
      }
      while (this.mainLayer.grid[testY][testX] != 'empty' || this.overLayer.grid[testY][testX] != 'empty');
      this.bonusBox[i] = new AnimatedSprite('bonusBox', {x: testX, y: testY}, imageRepository.get('bonusBox'), 10, 40, 0);
      this.overLayer.push(this.bonusBox[i]);
    }
    
    // Add the bonuses
    imageRepository.add('bomb', 'images/bomb48.png');
    this.bonus[0] = new AnimatedSprite('bomb', {x: 0, y: 0}, imageRepository.get('bomb'), 6, 500, 1);
    imageRepository.add('star', 'images/star48.png');
    this.bonus[1] = new Sprite('star', {x: 0, y: 0}, imageRepository.get('start'));
    
    // Define explosion
    imageRepository.add('explosion', 'images/explosion.png');
    this.explosion = new AnimatedSprite('explosion', {x: 0, y: 0}, imageRepository.get('explosion'), 16, 50, 1);
    this.explosion.collision = new Collision(this.explosion);
    
    // Add gates
    imageRepository.add('gateClose', 'images/gateclose48.png');
    imageRepository.add('gateOpen', 'images/gateopen48.png');
    for (var i in this.players)
    {
      this.gates[i] = new AnimatedSprite('gate', this.players[i].position, imageRepository.get('gateClose'), 6, 400, 1);
    }

    // Launch the first animation : Character move out the gate and the gate closes  
    for (var i in this.players)
    {
      this.players[i].axion.actionList.autoMove.active = true;
      this.players[i].axion.actionList.autoMove.timer = 50;
      this.players[i].axion.actionList.autoMove.start = this.players[i].position;
      switch (this.players[i].direction) {
        case 'W' : 
          this.players[i].axion.actionList.autoMove.end = {x: this.players[i].position.x + 1, y: this.players[i].position.y};
          break;
        case 'E' : 
          this.players[i].axion.actionList.autoMove.end = {x: this.players[i].position.x - 1, y: this.players[i].position.y};
          break;
        case 'N' : 
          this.players[i].axion.actionList.autoMove.end = {x: this.players[i].position.x, y: this.players[i].position.y + 1};
          break;
        case 'S' : 
          this.players[i].axion.actionList.autoMove.end = {x: this.players[i].position.x, y: this.players[i].position.y - 1};
          break;
      }
    }

    // Start the game
    imageRepository.start = true;
  }

  /**
   * Starting animation
   */
  this.startAnimation = function()
  {    
    if (self.gameAnimation.startTimerPlayer == 0)
    {
      self.gameAnimation.timerGate = self.gates[0].timer * self.gates[0].frames;
      self.gameAnimation.timerPlayer = this.players[0].axion.actionList.autoMove.timer * 12;
      self.gameAnimation.startTimerPlayer = Date.now();
    }
    else if (self.gameAnimation.startTimerGate == 0 && self.gameAnimation.startTimerPlayer + self.gameAnimation.timerPlayer < Date.now())
    {
      for (var i in self.gates)
      {
        self.mainLayer.push(self.gates[i]);
      }
      self.gameAnimation.startTimerGate = Date.now();
    }
    else if (self.gameAnimation.startTimerGate > 0 && self.gameAnimation.startTimerGate + self.gameAnimation.timerGate < Date.now())
    {
      self.gameAnimation.state = 'on-going';
      self.gameAnimation.startTimerGate = 0;
      self.gameAnimation.startTimerPlayer = 0;         
      self.enableAction = true;
    }
  }
  
  /**
   * Starting animation
   */
  this.finishAnimation = function(gate)
  {
    if (self.gameAnimation.startTimerGate == 0)
    {
      self.enableAction = false;
      self.gameAnimation.winner.axion.actionList.move.active = false;
      switch (self.gameAnimation.winner.direction)
      {
        case 'N' : 
          self.gameAnimation.winner.axion.actionList.autoMove.end = {x: self.gameAnimation.winner.position.x, y: self.gameAnimation.winner.position.y - 1};
          self.mainLayer.removeFrom(self.gameAnimation.winner.position.x, self.gameAnimation.winner.position.y - 1);
          break;
        case 'S' : 
          self.gameAnimation.winner.axion.actionList.autoMove.end = {x: self.gameAnimation.winner.position.x, y: self.gameAnimation.winner.position.y + 1};
          self.mainLayer.removeFrom(self.gameAnimation.winner.position.x, self.gameAnimation.winner.position.y + 1);
          break;
        case 'W' :
          self.gameAnimation.winner.axion.actionList.autoMove.end = {x: self.gameAnimation.winner.position.x - 1, y: self.gameAnimation.winner.position.y};
          self.mainLayer.removeFrom(self.gameAnimation.winner.position.x - 1, self.gameAnimation.winner.position.y);
          break;
        case 'E' : 
          self.gameAnimation.winner.axion.actionList.autoMove.end = {x: self.gameAnimation.winner.position.x + 1, y: self.gameAnimation.winner.position.y};
          self.mainLayer.removeFrom(self.gameAnimation.winner.position.x + 1, self.gameAnimation.winner.position.y);
          break;
      }
      this.endGate = new AnimatedSprite('gate', self.gameAnimation.winner.axion.actionList.autoMove.end, imageRepository.get('gateOpen'), 6, 400, 1);
      self.mainLayer.push(this.endGate);
      self.gameAnimation.timerGate = this.endGate.timer * this.endGate.frames;
      self.gameAnimation.startTimerGate = Date.now();
      console.log(' --> ' + this.endGate.timer)
      console.log(' ---> ' + this.endGate.frames)
    }
    else if (self.gameAnimation.startTimerPlayer == 0 && self.gameAnimation.startTimerGate + self.gameAnimation.timerGate < Date.now())
    {
      self.gameAnimation.winner.axion.actionList.autoMove.active = true;
      self.gameAnimation.winner.axion.actionList.autoMove.start = self.gameAnimation.winner.position;
      self.gameAnimation.winner.axion.actionList.autoMove.timer = 50;
      self.gameAnimation.timerPlayer = self.gameAnimation.winner.axion.actionList.autoMove.timer * 12;
      console.log(self.gameAnimation.winner.axion.actionList);
      self.gameAnimation.startTimerPlayer = Date.now();
    }
    else if (self.gameAnimation.startTimerPlayer > 0 && self.gameAnimation.startTimerPlayer + self.gameAnimation.timerPlayer < Date.now())
    {
      console.log(self.gameAnimation.winner.name + ' wins !');      
      self.play = false;
    }
  }

  /**
   * Redraw all the sprites in the canvas
   */
  this.draw = function()
  {

    // Clean the canvas
    context.clearRect(0, 0, canvas.width, canvas.height);
    
    // Draw the path
    if (this.showPath && "items" in this.showPath)
    {
      for(var i in this.showPath.items) 
      {
        context.fillRect(this.showPath.items[i].x * spriteSize + self.scroll.x + Math.floor(spriteSize / 4), this.showPath.items[i].y * spriteSize + self.scroll.y +  Math.floor(spriteSize / 4), Math.floor(spriteSize / 2), Math.floor(spriteSize / 2));
        this.showPath.items
      }
      if ((this.showPath.startTime+3000) < Date.now()) {
        this.showPath = null;
      }
    }
    
    // Draw each overlay sprites on the canvas
    for (var i in self.overLayer.elements)
    {
			self.overLayer.elements[i].update();
      self.overLayer.elements[i].draw(canvas, self.scroll);
    }    

    // Draw each sprites on the canvas
    for (var i in self.mainLayer.elements)
    {
			self.mainLayer.elements[i].update();
      self.mainLayer.elements[i].draw(canvas, self.scroll);
    }    
  }
}
